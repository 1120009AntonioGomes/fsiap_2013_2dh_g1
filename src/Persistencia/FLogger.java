package Persistencia;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FLogger extends Logger {

    public FLogger(String arg0, String arg1) {
        super(arg0, arg1);
        this.setLevel(Level.FINER);
    }
}
