package Persistencia;

import java.io.IOException;
import java.util.logging.FileHandler;

public class AFileHandler extends FileHandler {

    public AFileHandler() throws IOException, SecurityException {
        super();
    }

    public AFileHandler(String arg0) throws IOException, SecurityException {
        super(arg0);
    }

    public AFileHandler(String arg0, boolean arg1) throws IOException,
            SecurityException {
        super(arg0, arg1);
    }

    public AFileHandler(String arg0, int arg1, int arg2) throws IOException,
            SecurityException {
        super(arg0, arg1, arg2);
    }

    public AFileHandler(String arg0, int arg1, int arg2, boolean arg3)
            throws IOException, SecurityException {
        super(arg0, arg1, arg2, arg3);
    }
}
