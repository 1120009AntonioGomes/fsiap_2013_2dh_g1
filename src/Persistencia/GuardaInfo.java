/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import calc.Prefixo;
import calc.Carga;
import calc.Calculo;

public class GuardaInfo implements Serializable {

    /**
     * Array de prefixos para tentar implementar
     *
     * @see #http://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:Prefixos_do_SI
     * @see Prefixo
     */
    public static Prefixo[] prefixos = new Prefixo[9];

    /**
     * Array para guardar cargas
     *
     * @see #getCarga(int)
     * @see #addCharge(Charge)
     * @see #removeCharge(Charge)
     * @see #removeCargaPorIndice(int)
     * @see #limpaCargas()
     */
    private ArrayList<Carga> cargas = new ArrayList<Carga>();

    public int chargeScale = 1, potencialLinhaCarga = 2, plinhas = 5, indiceprefixo = 4,
            tickSpacing = 50;

    public long gradMax = 81000, gradMin = 900, potOffset = 200000,
            potspacing = 350000;

    public Color corGrelha = null, infoAtras = null, corNeg = null,
            corPos = null, corPotencial = null, texto = null,
            corRato = null, backgroundColor = null;

    public boolean grelha = true, ticksOn = true, fieldLinesOn = true,
            potentialLinesOn = false;

    public int mainWinWidth = 800, MainWinHeight = 600;

    private int densidade = 16;

    public double totalCargaPositiva = 0, FactorSistemaInternacional = 1; // conversion factor
    // in pixels/meter

    /**
     * Creates new GuardaInfo object.
     *
     * @see #defineCores()
     * @see #criaArrayDePrefixos()
     */
    public GuardaInfo() {
        defineCores();
        criaArrayDePrefixos();
    }

    /**
     * Adds a charge to the cargas ArrayList
     *
     * @see #cargas
     * @param c Charge to add to list
     */
    public void adicionaCarga(Carga c) {
        cargas.add(c);
    }

    /**
     * Clears all charges from the cargas ArrayList and then calls the System
     * garbage collector
     *
     * @see #cargas
     */
    public void limpaCargas() {
        cargas.clear();
        System.gc();
    }

    /**
     * Gets the charge from the cargas array list at the given index.
     *
     * @param index cargas ArrayList item to return
     * @return Charge
     * @see Charge
     * @see #cargas
     */
    public Carga getCarga(int index) {
        return cargas.get(index);
    }

    /**
     * Gets the index of Charge c.
     *
     * @param c Charge to find the index of
     * @return Index of the given Charge in the cargas ArrayList
     * @see #cargas
     */
    public int getIndiceCarga(Carga c) {
        return cargas.indexOf(c);
    }

    /**
     * Returns the length of the cargas ArrayList
     *
     * @return Number of cargas in the cargas ArrayList
     * @see #cargas
     */
    public int getNumCargas() {
        return cargas.size();
    }

    /**
     * Returns the cargas ArrayList as an Array
     *
     * @return Charge array of charges in cargas ArrayList
     * @see #cargas
     */
    public ArrayList<Carga> getCargas() {
        return cargas;
    }

    /**
     * Gets the Charge density
     *
     * @return Charge density (double)
     * @see #densidade
     */
    public int getDernsidadeCarga() {
        return densidade;
    }

    /**
     * O potencial de uma linha de carga pode ser encontrado por sobreposição
     * dos potenciais de carga pontuais de elementos de carga infinitesmal. É um
     * exemplo de uma distribuição de carga contínua.
     *
     * @return Potential resolution (integer)
     * @see #potencialLinhaCarga
     */
    public int getPotencialLinhadeCarga() {
        return potencialLinhaCarga;
    }

    /**
     * Gets the prefix corrsponding to the specificed power of ten Currently
     * returns only the 4th prefix (0)
     *
     * @param POT potência de 10
     * @return Prefix
     * @see Prefix
     */
    public Prefixo getPrefixo(int POT) {
        /*
         * currently hacked to only return the no prefix prefix
         */
        return prefixos[4];
    }

    /**
     * Gets the index of the current Prefix
     *
     * @return Index of current Prefix
     * @see #prefixos
     */
    public int getIndicePrefixo() {
        return indiceprefixo;
    }

    /**
     * Devolve a escala do prefixo
     *
     * @return Scaler of current prefix.
     * @see Prefix#mult()
     */
    public double getEscala() {
        return prefixos[indiceprefixo].mult();
    }

    /**
     * Inverte a Cor
     *
     * @param in cor inicial
     * @return inverso dessa cor
     */
    public Color inverte(Color in) {
        Color novaCor = null;
        novaCor = new Color(255 - in.getRed(), 255 - in.getGreen(), 255 - in
                .getGreen(), 50);

        return novaCor;
    }

    /**
     * Calls reBuildShape on all charges.
     *
     * @see Charge#reBuildShape()
     */
    public void rebuildAllShapes() {
        for (Carga i : getCargas()) {
            if (i != null) {
                i.reBuildShape();
            }
        }
    }

    /**
     * Removes a charge from the cargas ArrayList
     *
     * @param c Charge to be removed
     * @see #cargas
     */
    public void removeCarga(Carga c) {
        cargas.remove(c);
    }

    /**
     * Removes a charge from the cargas ArrayList based on the index number
     *
     * @param index Index of charge to be removed
     * @see #cargas
     */
    public void removeCargaPorIndice(int index) {
        cargas.remove(index);
    }//NAO FUNCIONA

    /**
     * Resets program variables to original values
     *
     * @see #defineCores()
     * @see #actualizaEscalas()
     *
     */
    public void limpaTUDO() {
        this.defineCores();
        FactorSistemaInternacional = 1;
        potencialLinhaCarga = 2;
        plinhas = 5;
        tickSpacing = 50;
        ticksOn = true;
        indiceprefixo = 4;
        grelha = true;
        actualizaEscalas();
    }

    /**
     * Sets the Charge Density
     *
     * @param novaDensidade New charge density
     * @see #densidade
     */
    public void setDensidadeCarga(int novaDensidade) {
        this.densidade = novaDensidade;
    }

    /**
     * Sets the Potential Resolution
     *
     * @param potencialLCarfa New potential resolution
     * @see #potencialLinhaCarga
     */
    public void setPotencialLinhaDeCarga(int potencialLCarfa) {
        this.potencialLinhaCarga = potencialLCarfa;
    }

    /**
     * Sets the current prefix index.
     *
     * @param x new prefix index; must be between 8 and 0 inclusive.
     */
    public void setPrefixo(int x) {
        x = Math.min(8, x);
        x = Math.max(0, x);
        indiceprefixo = x;
    }

    /**
     * Sets all colors to defaults.
     */
    public void defineCores() {
        corRato = Color.BLACK;
        corPotencial = new Color(200, 255, 200);
        texto = new Color(250, 250, 255);
        backgroundColor = Color.WHITE;
        corNeg = Color.GREEN;
        corPos = Color.RED;
        corGrelha = Color.BLACK;
        infoAtras = Color.BLUE;
    }

    /**
     * initalizes the prefix array
     *
     * @see Prefixo
     */
    public void criaArrayDePrefixos() {
        prefixos[8] = new Prefixo("tera", "T", 12);
        prefixos[7] = new Prefixo("giga", "G", 9);
        prefixos[6] = new Prefixo("mega", "M", 6);
        prefixos[5] = new Prefixo("kilo", "k", 3);
        prefixos[4] = new Prefixo("", "", 0);
        prefixos[3] = new Prefixo("centi", "c", -2);
        prefixos[2] = new Prefixo("milli", "m", -3);
        prefixos[1] = new Prefixo("micro", "u", -6);
        prefixos[0] = new Prefixo("nano", "n", -9);
    }

    /**
     * Updates the potential offset and spacing, field line color scale and
     * field line distrobution.
     */
    public void actualizaEscalas() {
        double maxP = Double.MIN_VALUE, maxF = Double.MIN_VALUE, posTotal = 0;
        for (Carga temp : getCargas()) {
            double tcharge = temp.getCarga();
            if (tcharge > 0) {
                posTotal += tcharge;
            }
            double[] t = Calculo.CalcCampoEletrico((int) temp.getX() + 3,
                    (int) temp.getY() + 3);
            double newP = t[0];
            maxP = Math.max(maxP, newP);
            double newF = t[1];
            maxF = Math.max(maxF, newF);
        }
        if (maxP > 0) {
            this.potOffset = (long) maxP / 8;
            this.potspacing = potOffset / plinhas;
        }
        if (maxF > 0) {
            this.gradMax = (long) maxF / 10;
            this.gradMin = (long) Math.sqrt(this.gradMax * 10);
        }
        this.totalCargaPositiva = posTotal;
    }
}
