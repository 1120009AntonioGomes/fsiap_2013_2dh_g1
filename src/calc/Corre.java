package calc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

import Persistencia.AFileHandler;
import Persistencia.GuardaInfo;
import Persistencia.ILogger;
import Persistencia.Lev;
import Persistencia.SLogger;
import gui.Main;
import gui.Mensagens;
import gui.EmCima;

/*PERSISTENCIA DESTA CLASSE BASEADA NO TUTORIAL:

 http://tutorials.jenkov.com/java-logging/handlers.html
 http://docs.oracle.com/javase/7/docs/api/java/util/logging/Handler.html
 entre outros

 */
public class Corre {

    /**
     * File Handler para login
     *
     * @see AFileHandler
     */
    public static AFileHandler aH = null;

    /**
     *
     *
     * @see SLogger
     */
    public static SLogger critical = null;

    /**
     * GuardaInfo classe para guardar a informacao e fazer PERSISTENCIA - cria
     * objecto do tipo GuardaInfo "dat" que trata tudo
     *
     * @see GuardaInfo
     */
    public static GuardaInfo dat = null;

    /**
     *
     *
     * @see ILogger
     */
    public static ILogger info = null;

    /**
     * Ligado desligado para fazer log
     */
    public static boolean LogOn = true;

    /**
     * Instance of Main janeladow
     *
     * @see Main
     */
    public static Main janela = null;

    public static void Exit(int State) {
        if (LogOn) {
            critical.log(Lev.SEVERE, "FECHAR IRREGULAR: " + State);
            System.exit(State);
        } else {
            janela.dispose();
            dat = null;
        }
    }

    /**
     * Carrega do Ficheiro
     */
    public static GuardaInfo Load(String file) {
        ObjectInputStream ois = null;
        FileInputStream fis = null;
        Object temp = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            if (LogOn) {
                info.log(Lev.FINE, "erro a criar o inputstream", e);
            }
            return null;
        }
        try {
            ois = new ObjectInputStream(fis);
        } catch (IOException e1) {
            if (LogOn) {
                info.log(Lev.FINE, "erro ao criar o objectoinput", e1);
            }
            return null;
        }
        try {
            temp = (GuardaInfo) ois.readObject();
        } catch (IOException e2) {
            if (LogOn) {
                info.log(Lev.WARNING, "erro ao ler o obejcto", e2);
            }
            return null;
        } catch (ClassNotFoundException e2) {
            if (LogOn) {
                info.log(Lev.FINE, "Nao conseguiu criar Data pelo fich"
                        + ", ficheiro invalido", e2);
            }
            return null;
        }
        if (LogOn) {
            info.log(Lev.INFO, "Data carregada pelo ficheiro", dat);
        }
        GuardaInfo rv = (GuardaInfo) temp;

        return rv;
    }

    /**
     * LOGIN do programa
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        for (String i : args) {
            if (i.equals("LogIn Falhou")) {
                LogOn = false;
            }
        }
        /*
         * Cria Janela EmCima (literalmente em cima) - so enquanto carrega
         */
        EmCima spl = null;

        if (LogOn) {
            
            info = new ILogger("Informacao de persistencia", null);
            critical = new SLogger("persistencia critica", null);
            try {
                aH = new AFileHandler("Ficheio de persistencia", 10485760, 10, true);
            } catch (SecurityException e) {
                e.printStackTrace();
                Exit(2);
            } catch (IOException e) {
                e.printStackTrace();
                Exit(2);
            }
            /*
             * inicializa os registos de persistencia
             */
            aH.setFormatter(new SimpleFormatter());
            info.addHandler(aH);
            critical.addHandler(aH);
            info.log(Level.INFO, "Iniciando a aplicacao");
        }
        /*
         * Carrega pelo Fich
         */

        GuardaInfo temp = null;
        try {
            temp = Load("state.o");
        } catch (java.security.AccessControlException e) {
        }
        if (temp != null) {
            dat = temp;
        } else {
            dat = new GuardaInfo();
        }
        dat.criaArrayDePrefixos();

        /*
         * cria a janela principal
         */
        try {
            janela = new Main();
        } catch (RuntimeException e1) {
            if (LogOn) {
                critical.log(Lev.SEVERE, "Nao conseguiu criar janela principal", e1);
            }
            Exit(1);
        }
        dat.rebuildAllShapes();
        janela.setVisible(true);
        if (spl != null) {
            spl.setVisible(false);
            spl.dispose();
        }
    }

    /**
     * Salvao ficheiro dat com a informacao - FICHEIRO BINARIO try-catch segundo
     * tutorial
     */
    public static void Salva(String file) {
        ObjectOutputStream oos = null;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            if (LogOn) {
                info.log(Lev.FINE, "erro a criar o inputstream", e);
            }
            return;
        }
        try {
            oos = new ObjectOutputStream(fos);
        } catch (IOException e1) {
            if (LogOn) {
                info.log(Lev.FINE, "erro ao criar objectoinput", e1);
            }
            return;
        }
        try {
            oos.writeObject(dat);
        } catch (IOException e2) {
            if (LogOn) {
                info.log(Lev.WARNING, "erro I/O ao salvar", e2);
            }
            return;
        }
        if (LogOn) {
            info.log(Lev.FINE, "Data written to file");
        }
    }
}
