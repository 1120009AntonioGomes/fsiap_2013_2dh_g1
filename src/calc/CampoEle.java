/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calc;



public class CampoEle {

    private double campo;
    
    public CampoEle() {
    }
    
    public CampoEle(double campo){
        this.campo = campo;
    }
    
    public double getCampo(){
        return campo;
    }
    
    public void setCampo(double campo){
        this.campo = campo;
    }
    
}
