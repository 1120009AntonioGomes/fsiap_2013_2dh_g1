/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class CargaPontual extends Carga {

    public CargaPontual(double carga, double X, double Y) {
        this.setTipo(Carga.Tipo.CargaPontual);
        this.setCarga(carga);
        this.setX((int) X);
        this.setY((int) Y);
        this.reBuildShape();
    }

    public CargaPontual(double carga, int X, int Y) {
        this.setTipo(Carga.Tipo.CargaPontual);
        this.setCarga(carga);
        this.setX(X);
        this.setY(Y);
        this.reBuildShape();
    }

    /**
     * Checks to seen if a point is inside of the charge as it is drawn on the s
     *
     * @param x X Location of point to check.
     * @param y Y Location of point to check.
     * @return boolean True if point (x,y) is covered by this when it is
     * painted, otherwise false.
     */
    public boolean isInside(double x, double y) {
        if (Calculo.dist(x, y, this.getX(), this.getY()) < (this.getTamanho() / 2)) {
            return true;
        }
        return false;
    }

    /**
     * Draws this on screen with Graphics g (cast to a Graphics 2D internally).
     * Sets color based on charge sign. Draws this.myShape.
     *
     * @param g
     */
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        if (this.getCarga() > 0) {
            g2.setColor(Corre.dat.corPos);
        } else {
            g2.setColor(Corre.dat.corNeg);
        }
        g2.fill(this.myShape);
        if (isSel) {
            g2.setColor(java.awt.Color.WHITE);
            g2.draw(this.myShape);
        }
        g2.setColor(Corre.dat.texto);
        g2.drawString("" + this.getCarga(), this.getDesenhoX() - 9, this
                .getDesenhoY() + 4);
    }

    /**
     * Creates Shape myShape - biblioteca do java
     *
     * @see Charge#myShape
     */
    public void reBuildShape() {
        double s = getTamanho(), offSet = (s / 2), dx = this.getDesenhoX() - offSet, dy = this
                .getDesenhoY()
                - offSet;
        this.myShape = new gui.FormaCargaPontual(dx, dy, this.getTamanho()); //falta implementar a forma - implementado com Elipse 2D!
    }

    /**
     * @return String da carga pontual
     */
    public String toString() {
        return "Carga Pontual: " + getTextoCarga() + " (" + (getDesenhoX()) + ","
                + (getDesenhoY()) + ")";
    }
}
