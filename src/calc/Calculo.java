package calc;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

import java.awt.Color;
import java.awt.Graphics2D;

import Persistencia.FLogger;

public final class Calculo {

    public static FLogger fLog = null;

    /**
     * grossura das linhas para se vêrem apresentadas
     */
    public static final double grossura = 6;

    /**
     * Constante de coulomb
     */
    public static final double k = 8.99e9;

    /**
     * Calcular o Campo Eletrico em qualquer ponto
     *
     * @param x Localizacao em X (pixeis)
     * @param y Localizacao em Y (pixeis)
     * @return Array com dois valores,o primeiro valor 0 é a energia potencial
     * nesse ponto, e o valor 1 é o campo eletrico em x,y
     */
    public static double[] CalcCampoEletrico(int x, int y) {
        double PE = 0, forcaemX = 0, forcaemY = 0;
        double[] rv = new double[2];
        for (Carga temp : Corre.dat.getCargas()) {
            double tX = temp.getX(), tY = temp.getY(), dist = dist(tX, tY, x, y), leicoulomb = LeiDeCoulomb(
                    temp.getCarga(), dist);
            forcaemY += (leicoulomb * ((tY - y) / (dist)));
            forcaemX += (leicoulomb * ((tX - x) / (dist)));
            PE += potencial(temp.getCarga(), dist);
        }
        rv[0] = PE;
        rv[1] = sqrt((forcaemX * forcaemX) + (forcaemY * forcaemY));
        return rv;
    }

    /**
     * Coulomb's law
     *
     * @param carga Carga Pontual
     * @param distEntreCargas Distancia da carga para calcular a forca (em
     * pixeis)
     * @return double, forca em Newtons exercida pela carga
     */
    public static double LeiDeCoulomb(double carga, double distEntreCargas) { //PODIAMOS METER ISTO A EXPORTAR PARA A CALCULADORA!!!!!!
        distEntreCargas /= Corre.dat.FactorSistemaInternacional; // fator de conversao
                /* /=    Divide o esquerdo com o direito e atribuir o resultado ao esquerdo
         */
        return (k * -carga) / (distEntreCargas * distEntreCargas);
    }

    /**
     * Teorema de Pitágoras
     *
     * @param x1 X no primeiro elemnto
     * @param y1 Y no primeiro elemento
     * @param x2 X no segundo elemento
     * @param y2 Y no segundo elemento
     * @return devolve a distância entre dois pontos
     */
    public static double dist(double x1, double y1, double x2, double y2) {
        return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
    }

    /**
     * Desenha uma curva a representar a linha de campo eletrico feita pelas
     * cargas
     *
     * @param x X - localizacao para começar a desenhar
     * @param y Y - localizacao para começar a desenhar
     * @param g Graphics2D usados para desenhar -
     * http://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html
     */
    public static void DesenharLinha(int x, int y, Graphics2D g) {
        /* current x */
        int cX = x;
        /* current y */
        int cY = y;
        /* next x */
        int nX = 0;
        /* next y */
        int nY = 0;
        /* percentage change in x and y values (between -1 and +1) */
        double xper = 0, yper = 0;
        do {
            double[] temp = totalForce2(cX, cY);
            xper = temp[1] / temp[0];
            yper = temp[2] / temp[0];
            nX = cX + (int) (grossura * xper);
            nY = cY + (int) (grossura * yper);
            /*método graphics2d*/ g.setColor(CalculaCorPorDist(Corre.dat.gradMin, Corre.dat.gradMax, abs(temp[1])));
            /*método graphics2d*/ g.drawLine(cX, cY, nX, nY);
            cX = nX;
            cY = nY;
        } while (abs(cX) < 1600 && abs(cY) < 1600 && !prox(cX, cY)); //isto é enquanto dentro dos limites
    }

    /**
     * Desenha uma linha onde os pontos nessa linha têm o mesmo potencial
     * Eletrico
     *
     * @param valor Potencial ao qual desenhar a linha
     * @param g1 Graphics2D para desenhar
     */
    public static void DesenharLinhaMesmoPotencialE(double valor, Graphics2D g1) {
        int w = Corre.janela.getLarguraPainel(), h = Corre.janela.getAlturaPainel(), res = Corre.dat.getPotencialLinhadeCarga();
        g1.setColor(Corre.dat.corPotencial);
        double multiMin = 1e-6, multMax = 2, fMin = 50, rmult = 2.5e7, fMax = 5e7;
        double multiDist = (multMax - multiMin), mult = .5, forcadist = (fMax - fMin), fPer = .5, distmax = rmult * mult;
        for (int x = 0; x < w; x += res) {
            for (int y = 0; y < h; y += res) {
                double[] var = CalcCampoEletrico(x, y);
                double dist = abs(abs(var[0]) - valor);
                fPer = (var[1] - fMin) / forcadist;
                distmax = rmult * fPer * multiDist;
                if (dist < distmax) {
                    g1.setColor(CalculaCorPorDist(0, distmax, dist));
                    g1.fillRect(x, y, res, res);
                }
            }
        }
    }

    /**
     * descobre o melhor prefixo para um numero
     *
     * @param val numero
     * @return Prefixo
     */
    public static Prefixo encontraPrefixo(double val) {
        int power = (int) Math.log10(abs(val));
        return Corre.dat.getPrefixo(power);
    }

    /**
     * Determina se o valor esta dentro da distancia maxima do valor alvo
     *
     * @param val valor para verificar
     * @param target valor alvo
     */
    public static boolean VerificaDistValoresPos_neg(double val, double target, double posneg) {
        boolean rv = false;
        if (((val + posneg) < target) && ((val - posneg) > target)) {
            return true;
        }
        return rv;
    }

    /**
     * Expressao da energia potencial
     *
     * @param carga double, carga (em Coulombs)
     * @param radius double, distancia da carga para calcular Energia potencial
     * @return double, Voltagem nessa distancia
     */
    public static double potencial(double carga, double dist) {//PODIAMOS POR ISTO A SAIR DA CALCULADORA
        dist /= Corre.dat.FactorSistemaInternacional; // fator de conversao - pixeis metros
        return (k * carga) / dist;
    }

    /**
     * Determina se um ponto esta demasiado perto
     *
     * @param x X - localizacao para confirmar
     * @param y Y - localizacao para confirmar
     */
    private static boolean prox(int x, int y) {
        for (Carga temp : Corre.dat.getCargas()) {
            if (temp.isInside(x, y)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calcula a Força num ponto em Newtons, usando o teorema de pitagoras
     *
     * @param x X Location to calculate the force at.
     * @param y Y Location to calculate the force at.
     * @return double, força no ponto x,y
     */
    public static double ForcaTotalEmNewtons(int x, int y) {
        double total = 0, yf = 0, xf = 0;
        for (Carga c : Corre.dat.getCargas()) {
            double tX = c.getX(), tY = c.getY(), dist = dist(tX, tY, x, y), leicoulomb = LeiDeCoulomb(
                    c.getCarga(), dist);
            yf += (leicoulomb * ((tY - y) / (dist)));
            xf += (leicoulomb * ((tX - x) / (dist)));
        }
        total = sqrt((xf * xf) + (yf * yf));
        return total;
    }

    /**
     * Devolve um array que contem a força em X, em Y e a forca totais
     *
     * @param x X Location to calculate the force at.
     * @param y Y Location to calculate the force at.
     * @return array totais
     */
    public static double[] totalForce2(int x, int y) {
        double[] totais = new double[3];
        double yf = 0, xf = 0;
        for (Carga temp : Corre.dat.getCargas()) {
            double tX = temp.getX(), tY = temp.getY(), dist = dist(tX, tY, x, y), claw = LeiDeCoulomb(
                    temp.getCarga(), dist);
            if (temp.getTipo() == Carga.Tipo.CargaPontual) {
                yf += (claw * ((tY - y) / (dist)));
                xf += (claw * ((tX - x) / (dist)));
            } else {
            }
        }
        totais[0] = sqrt((xf * xf) + (yf * yf));
        totais[1] = xf;
        totais[2] = yf;
        return totais;
    }

    /**
     * Devolve a Força Total e o ângulo
     *
     *
     * @param x X Location to calculate the force at.
     * @param y Y Location to calculate the force at.
     * @return array totais
     */
    public static double[] ForcaTotaleAngulo(int x, int y) {
        double[] totais = new double[2];
        double yf = 0, xf = 0;
        for (Carga c : Corre.dat.getCargas()) {
            double tX = c.getX(), tY = c.getY(), dist = dist(tX, tY, x, y), leicoulomb = LeiDeCoulomb(
                    c.getCarga(), dist);
            yf += (leicoulomb * ((tY - y) / (dist)));
            xf += (leicoulomb * ((tX - x) / (dist)));
        }
        totais[0] = sqrt((xf * xf) + (yf * yf));
        totais[1] = Math.toDegrees(Math.atan(xf / yf));
        return totais;
    }

    /**
     * Energia Potencial num Ponto
     *
     * @param x X Location
     * @param y Y Location
     * @return double, totais potencial enegry at point (x,y)
     */
    public static double EnergiaPotencialNumPonto(int x, int y) {
        double energiapotencial = 0;
        for (Carga temp : Corre.dat.getCargas()) {
            energiapotencial += potencial(temp.getCarga(), dist(x, y, temp.getX(), temp
                    .getY()));
        }
        return energiapotencial;
    }

    //TRUNCAR O NUMERO 
    public static String truncaParaString(double numero, int casas) {
        String rv = "";
        Prefixo p = encontraPrefixo(numero);
        String prefixado = p.prefixo;
        double numtrunc = (numero / p.mult());
        rv = truncaDecimal(numtrunc, casas) + " " + prefixado;
        return rv;
    }

    /**
     * TRUNCAR O NUMERO COM CASAS DECIMAIS DEFINIDAS
     */
    public static double truncaDecimal(double numero, int casadec) {
        double rv, pow = Math.pow(10, casadec);
        numero *= pow;
        double tempd = ((long) numero) / pow;
        rv = tempd;
        return rv;
    }

    /**
     * SUPOSTAMENTE DETERMINAVA A COR BASEADA NA DISTANCIA MAXIMA - em relacao
     * ao valor recebido - e até determina, mas nao sempre
     *
     * @param min valor min da distancia max
     * @param max valor max da distancia max
     * @param val valor que vai receber a cor
     * @return java.awt.Color
     */
    public static Color CalculaCorPorDist(double min, double max, double val) {
        int vermelho = 0, verde = 0, azul = 0;
        if (val >= max) {
            return Color.RED;
        }
        if (val < min) {
            return Color.BLUE;
        }
        double per = (val - min) / (max - min);
        vermelho = (int) (255 * per);
        azul = (int) (255 * (1 - per));
        verde = Math.min(vermelho, azul);
        return new Color(vermelho, verde, azul);
    }

    /**
     * Calcula o angulo no sentido do ponteiro dos relogios - Funcao atan()
     *
     * @param x X -localizacao
     * @param y Y -localizacao
     * @return angulo teta em graus
     */
    public static double vectorAngle(int x, int y) {
        double theta = Math.atan(ForcaemX(x, y) / ForcaemY(x, y));
        return Math.toDegrees(theta);
    }
//Método ATAN(): The method returns the arctangent of the specified double value.

    /**
     * Calcula forca em X - horizontal
     *
     * @param x X -localizacao
     * @param y Y -localizacao
     * @return forca em newtons
     */
    public static double ForcaemX(int x, int y) {
        double forcaX = 0;
        for (Carga temp : Corre.dat.getCargas()) {
            double tX = temp.getX();
            double tY = temp.getY();
            double dist = dist(x, y, tX, tY);
            if (temp.getTipo() == Carga.Tipo.CargaPontual) {
                forcaX += (LeiDeCoulomb(temp.getCarga(), dist) * ((tX - x) / (dist)));
            } else {
                forcaX += (LeiDeCoulomb(temp.getCarga(), dist) * ((tX - x) / (dist)));
            }
        }
        return forcaX;
    }

    /**
     * Calcula forca em y - vertical
     *
     * @param x X -localizacao
     * @param y Y -localizacao
     * @return forca em newtons
     */
    public static double ForcaemY(int x, int y) {
        double forcaY = 0;
        for (Carga temp : Corre.dat.getCargas()) {
            double tX = temp.getX();
            double tY = temp.getY();
            double dist = dist(x, y, tX, tY);
            if (temp.getTipo() == Carga.Tipo.CargaPontual) {
                forcaY += (LeiDeCoulomb(temp.getCarga(), dist) * ((tX - x) / (dist)));
            } else {
                forcaY += (LeiDeCoulomb(temp.getCarga(), dist) * ((tX - x) / (dist)));
            }
        }
        return forcaY;
    }
}
