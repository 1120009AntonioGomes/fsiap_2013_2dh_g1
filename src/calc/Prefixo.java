/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

public class Prefixo {

    public int ValorE = 0;

    public String nome = "";

    public String prefixo = "";

    public double val = 0;

    public Prefixo(String n, String p, int pow) {
        ValorE = pow;
        nome = n;
        prefixo = p;
        val = Math.pow(10, ValorE);
    }

    public double mult() {
        return val;
    }

    public String toString() {
        return nome + " (" + prefixo + ")";
    }
}
