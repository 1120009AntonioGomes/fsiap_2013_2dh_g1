/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

/**
 *
 * @author i091206
 */
public class CargaCalc {
     private double vel; //velocidade da carga

    private double angulo; //angulo com que foi introduzida a carga

    /**
     * Carga em Coulombs
     */
    private double carga;


    public CargaCalc(double vel, double angulo, double carga){
       this.vel = vel;
       this.angulo=angulo;
       this.carga = carga;
        
    }
    public void setVelocidade(double vel){
        this.vel = vel;
    } 
    
    public double getAngulo(){
        return angulo;
    }
    
       public double getVel(){
        return vel;
    }
    
    
     public void setAngulo(double angulo){
        this.angulo = angulo;
    } 
public double getCarga(){
    return carga;
}
public void setCarga(double carga) {
        this.carga = (carga);
    }
    @Override
    public String toString() {
        return "carga " + carga + "velocidade" + vel + "angulo" + angulo; //To change body of generated methods, choose Tools | Templates.
    }
     
    
}
