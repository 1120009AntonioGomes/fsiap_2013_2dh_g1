package calc;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.io.Serializable;
import calc.Corre;

public abstract class Carga implements Comparable, Serializable {

    public static enum Tipo {

       CargaPontual; //nao sabia se ia ter outra representacao da crga
    }

    private double vel; //velocidade da carga

    private double angulo; //angulo com que foi introduzida a carga

    
   
    
    
    /**
     * Carga em Coulombs
     */
    private double carga;

    /**
     * selecionado ou nao
     *
     */
    public boolean isSel = false;

    /**
     * Shape é uma classe do Java -
     * http://docs.oracle.com/javase/7/docs/api/java/awt/Shape.html
     */
    protected Shape myShape;

    /**
     * Tipo da carga
     */
    private Tipo tipo = null;

    /**
     * Localizacao nos X em pixeis
     */
    private double x;

    /**
     * Localizacao nos Y em pixeis
     */
    private double y;

    /**
     * construtor vazio
     */
    public Carga() {

    }
   
    
    /**
     * Mouse Event só para saber se foi clicado
     */
    public boolean verificaClicado(java.awt.event.MouseEvent e) {
        double size = getTamanho();
        double xLoc = e.getX();
        double yLoc = e.getY();
        boolean X = ((this.getDesenhoX() > (xLoc - size)) && (this.getDesenhoX() < (xLoc + size)));
        boolean Y = ((this.getDesenhoY() > (yLoc - size)) && (this.getDesenhoY() < (yLoc + size)));

        return (X && Y);
    }

    /**
     * Compara dois objectos carga, por valor
     */
    public int compareTo(Object obj) {
        if (obj == null) {
            return Integer.MAX_VALUE;
        }

        int temp = 0;
        temp = (int) (this.getCarga() - ((Carga) obj).getCarga());

        return temp;
    }

    /**
     * Desenha a carga mesmo a arrastar. Tem de ser lento (nao sei porque)
     */
    public void drawOutline(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        this.paint(g2);
    }

    /**
     * @return devolve carga
     */
    public double getCarga() {
        return carga;
    }

    /**
     * Devolve a carga como textoo
     *
     * @return String
     */
    public String getTextoCarga() {
        String rv = "";
        rv += (this.carga / Corre.dat.getEscala());
        rv += (Corre.dat.getPrefixo(0).prefixo + "C");//coulombs
        return rv;
    }

    /**
     * @return altura em double
     * @see #getTamanho()
     */
    public double getAlturaCarga() {
        return getTamanho();
    }

    /**
     * @return largura em double
     * @see #getTamanho() - é igual porque a carga e redonda
     */
    public double getLength() {
        return getTamanho();
    }

    public int getDesenhoX() {
        /*
         * vai buscar o X
         */
        return (int) (this.getX());
    }

    public int getDesenhoY() {
        /*
         * vai buscar o Y
         */
        return (int) (this.getY());
    }

    /**
     *
     * @return tamanho da carga
     */
    public double getTamanho() {
        return 20;
    }

    /**
     * @return devolve o tipo da carga
     */
    public Tipo getTipo() {
        return tipo;
    }

    /**
     * @return Localizacao em X
     */
    public double getX() {
        return this.x;
    }

    /**
     * @return Localizacao em Y.
     */
    public double getY() {
        return this.y;
    }

    /**
     *
     * @param x onde está no X
     * @param y onde esta no y
     * @return booleano para saber se a carga foi colocada entro da visualizacao
     */
    public abstract boolean isInside(double x, double y);

    /**
     * Metodo para pintar, da classe MyShape
     *
     * @see #mySh
     * @param g objecto G de grafico
     */
    public abstract void paint(Graphics g);

    /**
     * Define a forma com a classe MyShape
     *
     * @see #myShape
     */
    public abstract void reBuildShape();

    /**
     * @param carga set de carga
     */
    public void setCarga(double carga) {
        this.carga = (carga);
    }

    /**
     * @param tipo
     *
     */
    protected void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    /**
     * @param X set da localizacao
     */
    public void setX(double X) {
        X = Math.max(-1600.0, X);
        X = Math.min(X, 1600.0);
        this.x = X;
        this.reBuildShape();
    }

    /**
     * @param Y set da localizacao
     */
    public void setY(double Y) {
        Y = Math.max(-1600.0, Y);
        Y = Math.min(Y, 1600.0);
        this.y = Y;
        this.reBuildShape();
    }

    /**
     * toString()
     *
     * @return String
     */
    public String toStringShort() {
        return "(" + getTextoCarga() + ") at ("
                + (int) (x / Corre.dat.FactorSistemaInternacional) + ","
                + (int) (y / Corre.dat.FactorSistemaInternacional) + ")";

    }
}
