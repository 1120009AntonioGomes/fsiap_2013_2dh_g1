
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

import calc.CampoEle;
import calc.CampoMag;
import calc.Carga;
import calc.CargaCalc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Calculadora {

    private StringBuilder sb = new StringBuilder();
    private double valorLorentz;
    private CargaCalc q;
    private CampoEle CampoEletricoo;
    private CampoMag CampoMagneticoo;

    public void mandaHTML() throws IOException {
        getSb().append("<html>");
        getSb().append("<head>");
        getSb().append("<title>Exportacao de Calculadora");
        getSb().append("</title>");
        getSb().append("</head>");
        getSb().append("<body>" + getvalorLorentz() + "" + getvalorCarga());
        getSb().append("</body>");
        getSb().append("</html>");
        FileWriter fstream = new FileWriter("Exportacao.html");
        BufferedWriter out = new BufferedWriter(fstream);
        out.write(getSb().toString());
    }

    public Calculadora() {
    }

    public Calculadora(double valorLorentz, CargaCalc q, CampoEle CampoEletricoo, CampoMag CampoMagnetico) {
        this.valorLorentz = valorLorentz;
        this.q = q;
        this.CampoEletricoo = CampoEletricoo;
        this.CampoMagneticoo = CampoMagnetico;
    }

    public Calculadora(CargaCalc q, CampoEle CampoEletricoo, CampoMag CampoMagneticoo) {
        this.q = q;
        this.CampoEletricoo = CampoEletricoo;
        this.CampoMagneticoo = CampoMagneticoo;
        calcularValorLorentz();
    }

    public double getvalorLorentz() {
        return getValorLorentz();
    }

    public void setvalorLorentz(double valorLorentz) {
        this.setValorLorentz(valorLorentz);
    }

    public double getvalorCarga() {
        return getQ().getCarga();
    }

    public void calcularValorCarga() {
        double valorcarga = this.getvalorLorentz() / (getCampoEletrico().getCampo() + getQ().getVel() * getCampoMagnetico().getCampo() * Math.cos(getQ().getAngulo()));
        this.getQ().setCarga(valorcarga);
    }

    public double calcularValorCarga(double ForcaLorentz, double CampoElectrico, double CampoMagnetico, double Velocidade, double angulo) {
        double valorcarga = ForcaLorentz / (CampoElectrico+ Velocidade * CampoMagnetico * Math.cos(angulo));
        return valorcarga;
    }

    public double calcularCampoEletrico(double ForcaLorentz, double CampoMagnetico, double Velocidade, double Particula, double angulo) {
        double aux = ((ForcaLorentz / Particula) - (Velocidade * CampoMagnetico * Math.cos(angulo * Math.PI / 180)));
        return aux;
    }

    public void calcularCampoMagnetico() {
        this.CampoMagneticoo.setCampo((this.getValorLorentz() - this.q.getCarga()* this.CampoEletricoo.getCampo()) / (this.q.getVel()* this.q.getCarga()* Math.cos(this.q.getAngulo()* Math.PI / 180)));
    }

    public double calcularCampMagnetico(double ForcaLorentz, double CampoElectrico, double Velocidade, double Particula, double angulo) {
   
       double aux = ((ForcaLorentz - Particula * CampoElectrico) / Velocidade * Particula * Math.cos(angulo * Math.PI /180));
        return aux;
    }

    public void calcularValorLorentz() {
        this.setValorLorentz(q.getCarga() * (getCampoEletrico().getCampo() + (q.getVel() * getCampoMagnetico().getCampo()) * Math.cos(q.getAngulo()* Math.PI / 180)));
    }

    public double calcularValLorentz(double CampoElectricc, double CampoMagnetico, double Velocidade, double Particula, double angulo) {
        double aux1 = (Particula * (CampoElectricc) + (Velocidade * CampoMagnetico) * Math.cos(angulo)* Math.PI / 180);
        return aux1;
    }

    public void calcularVelocidadeCarga() {
        double veloc = ((this.getValorLorentz() - q.getCarga() * this.getCampoEletrico().getCampo()) / this.getCampoMagnetico().getCampo() * q.getCarga() * Math.cos(q.getAngulo()* Math.PI / 180));
        this.q.setVelocidade(veloc);
    }

    public double calcularVelocidadCarga(double ForcaLorentz, double CampoElectrico, double CampoMagnetico, double Particula, double angulo) {
        double veloc = (((ForcaLorentz - (Particula * CampoElectrico)) / (CampoMagnetico * Particula * Math.cos(angulo)* Math.PI / 180)));

        return veloc;
    }

    public void calcularAngulo() {
        this.q.setAngulo(Math.acos((this.getValorLorentz() - (this.q.getCarga() * this.getCampoEletrico().getCampo())) / (this.q.getVel() * this.getCampoMagnetico().getCampo() * Math.cos(this.q.getAngulo() * Math.PI/180))));
    }
    
    public double calcularAngulo(double ForcaLorentz, double CampoElectrico, double CampoMagnetico, double Velocidade, double Particula) {
        double aux = (Math.acos((this.getValorLorentz()- (this.q.getCarga() * this.getCampoEletrico().getCampo())) / (this.q.getVel() * this.getCampoMagnetico().getCampo() * Math.cos(this.q.getAngulo() * Math.PI/180))));
        return aux;
    }

    /**
     * @return the sb
     */
    public StringBuilder getSb() {
        return sb;
    }

    /**
     * @param sb the sb to set
     */
    public void setSb(StringBuilder sb) {
        this.sb = sb;
    }

    /**
     * @return the valorLorentz
     */
    public double getValorLorentz() {
        return valorLorentz;
    }

    /**
     * @param valorLorentz the valorLorentz to set
     */
    public void setValorLorentz(double valorLorentz) {
        this.valorLorentz = valorLorentz;
    }

    /**
     * @return the q
     */
    public CargaCalc getQ() {
        return q;
    }

    /**
     * @param q the q to set
     */
    public void setQ(CargaCalc q) {
        this.q = q;
    }

    /**
     * @return the CampoEletrico
     */
    public CampoEle getCampoEletrico() {
        return CampoEletricoo;
    }

    /**
     * @param CampoEletrico the CampoEletrico to set
     */
    public void setCampoEletrico(CampoEle CampoEletrico) {
        this.CampoEletricoo = CampoEletrico;
    }

    /**
     * @return the CampoMagnetico
     */
    public CampoMag getCampoMagnetico() {
        return CampoMagneticoo;
    }

    /**
     * @param CampoMagnetico the CampoMagnetico to set
     */
    public void setCampoMagnetico(CampoMag CampoMagnetico) {
        this.CampoMagneticoo = CampoMagnetico;
    }

    void calcularCampoEletrico() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
