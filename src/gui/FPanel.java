package gui;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import calc.Calculo;
import calc.Carga;
import gui.RedesenhaJanela;
import calc.Corre;
import Persistencia.Lev;

public class FPanel extends JPanel {

    private volatile Graphics2D o = null;

    private Image offScreen, offScreen1, Background;

    private RedesenhaJanela rdt = new RedesenhaJanela();

    public volatile boolean reDrawBackground = true, restartPaint = false;
    ;

	private int res = Corre.dat.getPotencialLinhadeCarga();

    public FPanel() {
        super();
        this.setDoubleBuffered(true);
        this.addKeyListener(Corre.janela);
        initialize();
    }

    private void clearScreen(Graphics2D c) {
        c.setColor(Corre.dat.backgroundColor);
        c.fillRect(0, 0, this.getWidth(), this.getHeight());
    }

    private void drawGrid(Graphics2D g) {
        g.setColor(Corre.dat.corGrelha);
        int ticSpacing = Corre.dat.tickSpacing;
        for (int x = 0; x < this.getWidth(); x += ticSpacing) {
            g.drawLine(x, 0, x, this.getHeight());
        }
        for (int y = 0; y < this.getHeight(); y += ticSpacing) {
            g.drawLine(0, y, this.getWidth(), y);
        }
    }

    private void drawLines(Graphics2D g) {
        double mult = 2 * Math.PI;
        for (Carga temp : Corre.dat.getCargas()) {
            double n = Corre.dat.getDernsidadeCarga();
            if (temp.getCarga() > 0) {
                n *= (temp.getCarga() / Corre.dat.totalCargaPositiva);
                double dSize = (temp.getTamanho() / 1.8);
                for (int i = 0; i <= n; i++) {
                    if (restartPaint) {
                        return;
                    }
                    double per = (i / n);
                    int x = (int) (dSize * cos(mult * per));
                    int y = (int) (dSize * sin(mult * per));
                    x += temp.getDesenhoX();
                    y += temp.getDesenhoY();
                    Calculo.DesenharLinha(x, y, g);//verfificar se faz bem o desenho
                }
            }
        }
    }

    public void drawOffScreen() {
        restartPaint = true;
        while (rdt.isAlive()) {
        }
        if (!rdt.isAlive()) {
            rdt = new RedesenhaJanela();
            rdt.start();
        }
    }

    private void drawP(Graphics2D g1) {
        int num = Corre.dat.plinhas;
        long spacing = Corre.dat.potspacing;
        long val = 0;
        for (int r = num; r > -0; r--) {
            if (restartPaint) {
                return;
            }
            val = Corre.dat.potOffset - (r * spacing);
            Calculo.DesenharLinhaMesmoPotencialE(val, g1);
            Calculo.DesenharLinhaMesmoPotencialE(-val, g1);
        }
    }

    private void drawPC(Graphics2D g) {
        for (int i = 0; i < Corre.janela.clique.size(); i++) {
            if (restartPaint) {
                return;
            }
            Calculo.DesenharLinhaMesmoPotencialE((Corre.janela.clique.get(i)).doubleValue(), g);
        }
    }

    private void drawScale(Graphics2D g) {
        int yOff = 12;
        int xOff = 2;
        double meters = 0.0;
        g.setColor(Corre.dat.corRato);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Corre.dat.texto);
        for (int x = 1; x < this.getWidth(); x++) {
            if ((x % Corre.dat.tickSpacing) == 0) {
                g.drawLine(x, yOff - 10, x, yOff + 5);
                if ((x % (Corre.dat.tickSpacing * 2)) == 0) {
                    meters = (x / Corre.dat.FactorSistemaInternacional);
                    String temp = "" + (int) meters + "m";
                    if (temp.length() > 4) {
                        temp = temp.substring(0, 4);
                    }
                    g.drawString(temp, x - 7, yOff);
                }
            }
        }
        meters = 0.0;
        for (int y = 1; y < this.getWidth(); y++) {
            if ((y % Corre.dat.tickSpacing) == 0) {
                g.drawLine(xOff, y, xOff + 5, y);
                if ((y % (Corre.dat.tickSpacing * 2)) == 0) {
                    meters = (y / Corre.dat.FactorSistemaInternacional);
                    String temp = "" + (int) meters;
                    if (temp.length() > 4) {
                        temp = temp.substring(0, 4);
                    }
                    g.drawString(temp, xOff + 2, y + 3);
                }
            }
        }
    }

    public synchronized void finishDraw() {
        clearScreen((Graphics2D) offScreen1.getGraphics());
        offScreen1.getGraphics().drawImage(offScreen, 0, 0, this);
    }

    private Image getBackgroundImage() {
        if (reDrawBackground) {
            reDrawBackground();
        }
        return this.Background;
    }

    public int getRes() {
        return this.res;
    }

    private void initialize() {
        this.setBackground(Corre.dat.backgroundColor);
        this.resetSize();
        if (offScreen != null) {
            this.o = (Graphics2D) offScreen.getGraphics();
        }
    }

    public void paint(Graphics g1) {
        Graphics2D g = (Graphics2D) g1;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        try {
            clearScreen(g);
            g.drawImage(offScreen1, 0, 0, this);
            for (Carga temp : Corre.dat.getCargas()) {
                if (temp != null) {
                    temp.paint(g);
                }
            }
        } catch (Throwable t) {
            Corre.critical.log(Lev.SEVERE,
                    "Erro: nao desenhou", t);
            Corre.Exit(1);
        }
    }

    public synchronized boolean rdtDraw() {
        restartPaint = false;
        Corre.janela.setStatusLabel("Redesenhar");
        if (o == null) {
            Corre.info.log(Lev.WARNING,
                    "Tentou desenhar sem info de graficos");
            return false;
        }
        this.res = Corre.dat.getPotencialLinhadeCarga();
        try {
            clearScreen(o);
            if (Corre.dat.grelha) {
                o.drawImage(getBackgroundImage(), 0, 0, this);
            }
            if (Corre.dat.fieldLinesOn) {
                drawLines(o);
                drawPC(o);
                if (restartPaint) {
                    return false;
                }
            }
            if (Corre.dat.potentialLinesOn) {
                drawP(o);
                if (restartPaint) {
                    return false;
                }
            }
            if (Corre.dat.ticksOn) {
                drawScale(o);
            }
        } catch (Throwable t) {
            Corre.critical.log(Lev.SEVERE,
                    "Erro", t);
            Corre.Exit(1);
        }
        Corre.janela.setStatusLabel("Pronto");
        return true;
    }

    private void reDrawBackground() {
        Graphics2D g = (Graphics2D) Background.getGraphics();
        clearScreen(g);
        if (Corre.dat.grelha) {
            drawGrid(g);
        }
        reDrawBackground = false;
    }

    public boolean resetSize() {
        int w = this.getWidth(), h = this.getHeight();
        if (w <= 0 || h <= 0) {
            return false;
        }
        if (offScreen == null || w != offScreen.getWidth(null)
                || h != offScreen.getHeight(null)) {
            try {
                offScreen = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            } catch (RuntimeException e) {
                return false;
            }
        }
        if (offScreen1 == null || w != offScreen1.getWidth(null)
                || h != offScreen1.getHeight(null)) {
            try {
                offScreen1 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            } catch (RuntimeException e) {
                return false;
            }
        }
        if (Background == null || w != Background.getWidth(null)
                || h != Background.getHeight(null)) {
            try {
                Background = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
                reDrawBackground = true;
            } catch (RuntimeException e) {
                return false;
            }
        }
        o = (Graphics2D) offScreen.getGraphics();
        return true;
    }
}
