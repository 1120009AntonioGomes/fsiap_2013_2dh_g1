/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JDialog;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import calc.Corre;

public class RemoveTodos extends JDialog {

    private JButton button = null;

    private JButton button1 = null;

    private JLabel label = null;

    private JPanel panel = null;

    public RemoveTodos(java.awt.Frame owner) {
        super(owner);
        initialize();
        this.setLocationRelativeTo(owner);
    }

    private JButton getButton() {
        if (button == null) {
            button = new JButton("Yes");
            button.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    calc.Corre.janela.removeAll();
                    Corre.dat.actualizaEscalas();
                    dispose();
                }
            });
        }
        return button;
    }

    private JButton getButton1() {
        if (button1 == null) {
            button1 = new JButton("No");
            button1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    dispose();
                }
            });
        }
        return button1;
    }

    private JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel();
            panel.add(getButton(), null);
            panel.add(getButton1(), null);
        }
        return panel;
    }

    public void initialize() {
        label = new JLabel();
        this.setSize(439, 99);
        this.setModal(true);
        this.setTitle("Confirmar Eliminacao");
        label.setText("Tem a certeza que deseja apagar todas as Cargas?");
        label.setHorizontalAlignment(JLabel.CENTER);
        this.add(getPanel(), java.awt.BorderLayout.SOUTH);
        this.add(label, java.awt.BorderLayout.CENTER);
    }
}
