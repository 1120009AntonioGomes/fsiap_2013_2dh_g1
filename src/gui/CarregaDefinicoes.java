/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.FileDialog;
import java.awt.Frame;

public class CarregaDefinicoes extends FileDialog {

    public Frame own = null;

    public CarregaDefinicoes(Frame owner) {
        super(owner);
        own = owner;
        initialize();
    }

    public CarregaDefinicoes(Frame owner, String title, int mode) {
        super(owner, title, mode);
        own = owner;
        initialize();
    }

    private void initialize() {
        this.setLocationRelativeTo(own);
        this.setDirectory(System.getProperty("user.dir"));
    }
}
