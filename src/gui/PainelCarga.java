package gui;

/**
 *
 * @author Utilizador
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import calc.Carga;

public class PainelCarga extends JPanel {

    private JTextField campoCarga = null;

    private JLabel etiquetaCarga = null;

    private JLabel etiquetaNosX = null;

    private JTextField pontoXemCampo = null;

    private JLabel etiquetaNosY = null;

    private JTextField pontoYemCampo = null;

    public PainelCarga() {
        super();
        initialize(); //metodo para inicializar a grid.
    }

    public PainelCarga(Carga c) {
        super();
        initialize();
        this.setCarga(c);
    }

    private JTextField getCampoCarga() {
        if (campoCarga == null) {
            campoCarga = new JTextField();
            campoCarga.setInputVerifier(new VerificaDouble());
            campoCarga.requestFocus(true);
        }
        return campoCarga;
    }

    public double getValorCarga() throws NumberFormatException {
        return Double.parseDouble(campoCarga.getText());
    }

    private JTextField getXLocationField() {
        if (pontoXemCampo == null) {
            pontoXemCampo = new JTextField();
            pontoXemCampo.setInputVerifier(new VerificaDouble());
        }
        return pontoXemCampo;
    }

    public double getXVal() throws NumberFormatException {
        return Double.parseDouble(pontoXemCampo.getText());
    }

    private JTextField getLocalizacaoemCampo() {
        if (pontoYemCampo == null) {
            pontoYemCampo = new JTextField();
            pontoYemCampo.setInputVerifier(new VerificaDouble());
        }
        return pontoYemCampo;
    }

    public double getYVal() throws NumberFormatException {
        return Double.parseDouble(pontoYemCampo.getText());
    }

    private void initialize() { 
        GridBagConstraints yLocationFieldConstraints = new GridBagConstraints();
        yLocationFieldConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        yLocationFieldConstraints.gridy = 2;
        yLocationFieldConstraints.weightx = 1.0;
        yLocationFieldConstraints.gridx = 1;

        GridBagConstraints xLocationFieldConstraints = new GridBagConstraints();
        xLocationFieldConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        xLocationFieldConstraints.gridy = 1;
        xLocationFieldConstraints.weightx = 1.0;
        xLocationFieldConstraints.gridx = 1;
        GridBagConstraints chargeFieldConstraints = new GridBagConstraints();
        chargeFieldConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        chargeFieldConstraints.gridy = 0;
        chargeFieldConstraints.weightx = 1.0;
        chargeFieldConstraints.gridx = 1;

        GridBagConstraints yLabelConstraints = new GridBagConstraints();
        yLabelConstraints.gridx = 0;
        yLabelConstraints.gridy = 2;

        GridBagConstraints xLabelConstraints = new GridBagConstraints();
        xLabelConstraints.gridx = 0;
        xLabelConstraints.gridy = 1;

        GridBagConstraints chargeLabelConstraints = new GridBagConstraints();
        chargeLabelConstraints.gridx = 0;
        chargeLabelConstraints.gridy = 0;

        etiquetaCarga = new JLabel("Carga: ");
        etiquetaNosX = new JLabel("X: ");
        etiquetaNosY = new JLabel("Y: ");

        this.setLayout(new GridBagLayout());
        this.setSize(300, 84);
        this.add(etiquetaCarga, chargeLabelConstraints);
        this.add(etiquetaNosX, xLabelConstraints);
        this.add(etiquetaNosY, yLabelConstraints);
        this.add(getCampoCarga(), chargeFieldConstraints);
        this.add(getXLocationField(), xLocationFieldConstraints);
        this.add(getLocalizacaoemCampo(), yLocationFieldConstraints);
    }

    public void setCarga(Carga c) {
        this.campoCarga.setText("" + c.getCarga());
        this.pontoXemCampo.setText("" + c.getX());
        this.pontoYemCampo.setText("" + c.getY());
    }

    public void setXY(double x, double y) {
        this.pontoXemCampo.setText("" + x);
        this.pontoYemCampo.setText("" + y);
    }

    public void setKeyListener(KeyListener k) {
        pontoXemCampo.addKeyListener(k);
        pontoYemCampo.addKeyListener(k);
        campoCarga.addKeyListener(k);
    }
}
