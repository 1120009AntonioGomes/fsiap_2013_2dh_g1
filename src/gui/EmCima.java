package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class EmCima extends JFrame {

    private JLabel label = null;

    public EmCima() {
        super();
        initialize();
    }

    private void initialize() {
        label = new JLabel();
        this.setSize(210, 97);
        this.setTitle("");
        this.setAlwaysOnTop(true);
        this.setFont(new java.awt.Font("", java.awt.Font.ITALIC, 24));
        label.setText("Carregar");
        label.setHorizontalAlignment(JLabel.CENTER);
        this.add(label, java.awt.BorderLayout.CENTER);
        this.setUndecorated(true);
        this.setLocationRelativeTo(null);
    }
}
