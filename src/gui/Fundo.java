/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Persistencia.ILogger;
import calc.Corre;

public class Fundo extends Thread {

    public static volatile boolean pauseReqested;

    public ILogger bLog = null;

    public volatile boolean paintOnce = false;

    private volatile boolean stopRequested;

    public volatile int timeInt = 1500;

    public long totalT;

    public Fundo(String name) {
        this.setName(name);
        this.setPriority(4);
        this.setDaemon(true);

    }

    public void run() {
        long lastPaint = Long.MIN_VALUE;
        long interval = 3000;
        while (!stopRequested) {
            if (pauseReqested) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            while (!pauseReqested) {
                if (System.currentTimeMillis() - lastPaint > interval) {
                    Corre.janela.reDraw();
                    lastPaint = System.currentTimeMillis();
                }
            }
        }
    }

    public void stopGracefully() {
        stopRequested = true;
        Thread.currentThread().interrupt();
    }
}
