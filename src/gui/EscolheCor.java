/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;

import calc.Corre;

public class EscolheCor extends JDialog implements ActionListener {

    private JButton button = null;

    private JButton button1 = null;

    public java.awt.Color cColor = new java.awt.Color(255, 255, 255);

    public JColorChooser jColorChooser = null;

    private JPanel panel = null;

    public int sel = 0;

    public EscolheCor(Frame owner) {
        super(owner);
        initialize();
        this.setLocationRelativeTo(owner);
    }

    public void actionPerformed(ActionEvent arg0) {
        String action = arg0.getActionCommand();
        if (arg0.getActionCommand().equals("OK")) {
            if (sel == 1) {
                Corre.dat.backgroundColor = jColorChooser.getColor();
                Corre.dat.corGrelha = Corre.dat.inverte(jColorChooser.getColor());
                calc.Corre.janela.janela.updateColors();//janela principal e janela opcoes
                calc.Corre.janela.repaint();
            } else if (sel == 2) {
                Corre.dat.corPos = jColorChooser.getColor();
                calc.Corre.janela.janela.updateColors();
                calc.Corre.janela.repaint();
            } else if (sel == 3) {
                Corre.dat.corNeg = jColorChooser.getColor();
                calc.Corre.janela.janela.updateColors();
                calc.Corre.janela.repaint();
            } else if (sel == 4) {
                Corre.dat.texto = jColorChooser.getColor();
                calc.Corre.janela.janela.updateColors();
                calc.Corre.janela.repaint();
            } else if (sel == 5) {
                Corre.dat.corPotencial = jColorChooser.getColor();
                calc.Corre.janela.janela.updateColors();
                calc.Corre.janela.repaint();
            }
            this.dispose();
        }
        if (action.equals("CANCELAR")) {
            this.dispose();
        }
    }

    private JButton getButton() {
        if (button == null) {
            button = new JButton("OK");
            button.addActionListener(this);
        }
        return button;
    }

    private JButton getButton1() {
        if (button1 == null) {
            button1 = new JButton("Cancelar");
            button1.setActionCommand("CANCELAR");
            button1.addActionListener(this);
        }
        return button1;
    }

    private JColorChooser getJColorChooser() {
        if (jColorChooser == null) {
            java.awt.Color temp = new java.awt.Color(0, 0, 0);
            if (sel == 1) {
                temp = calc.Corre.dat.backgroundColor;
            } else if (sel == 2) {
                temp = calc.Corre.dat.corNeg;
            } else if (sel == 3) {
                temp = calc.Corre.dat.corPos;
            }
            jColorChooser = new JColorChooser(temp);
            jColorChooser.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent e) {
                    cColor = jColorChooser.getColor();
                }
            });
        }
        return jColorChooser;
    }

    private JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel();
            panel.add(getButton(), null); // Generated
            panel.add(getButton1(), null); // Generated
        }
        return panel;
    }

    public void initialize() {
        this.setModal(true);
        this.setTitle("Color"); // Generated
        this.setSize(440, 330);
        this.add(getPanel(), java.awt.BorderLayout.SOUTH); // Generated
        this.add(getJColorChooser(), java.awt.BorderLayout.CENTER); // Generated
    }
}
