package gui;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Persistencia.GuardaInfo;
import calc.Corre;
import gui.EscolheCor;

public class JanelaOpcoes extends JDialog implements ActionListener,
        KeyListener {

    public Color background = null;

    private JLabel bgColor = null;

    private JButton button = null;

    private JButton button1 = null;

    private JButton button3 = null;

    private JButton button4 = null;

    private JButton button5 = null;

    private JButton cancelButton = null;

    private Choice choice = null;

    private JPanel colorPanel = null;

    private JTextField gradientMax = null;

    private JTextField gradientMin = null;

    private JCheckBox gridOnOff = null;

    private JTextField gridSpace = null;

    private JLabel label = null;

    private JLabel label1 = null;

    private JLabel label10 = null;

    private JLabel label11 = null;

    private JLabel label12 = null;

    private JLabel label13 = null;

    private JLabel label14 = null;

    private JLabel label15 = null;

    private JLabel label2 = null;

    private JLabel label3 = null;

    private JLabel label4 = null;

    private JLabel label5 = null;

    private JLabel label6 = null;

    private JLabel label7 = null;

    private JLabel label8 = null;

    private JLabel label9 = null;

    public Color neg = null;

    private JLabel cargaNeg = null;

    private JButton okButton = null;

    private JPanel okPanel = null;

    private JPanel optionsPanel = null;

    private java.awt.Frame owner = null;

    private JTextField pLines = null;

    private JTextField pOff = null;

    private JLabel posCharge = null;

    private JLabel potColor = null;

    private JTextField potResField = null;

    public Color poz = null;

    private JTextField pSpace = null;

    private JButton reset = null;

    private JTextField siConv = null;

    private JLabel textColor = null;

    private JCheckBox tickOnOff = null;

    public JanelaOpcoes(JFrame owner) {
        super(owner);
        initialize();
        this.owner = owner;
        this.setLocationRelativeTo(owner);
    }

    public void actionPerformed(ActionEvent arg0) {
        updateColors();
        String temp = arg0.getActionCommand();
        if (temp.equals("OK")) {
            this.Sync();
            this.dispose();
        } else if (temp.equals("CANCEL")) {
            this.dispose();
        } else if (temp.length() > 9) {
            if (temp.substring(0, 9).equals("PICKCOLOR")) {
                if (temp.substring(9).equals("B")) {
                    EscolheCor win = new EscolheCor(this.owner);
                    win.jColorChooser.setColor(Corre.dat.backgroundColor);
                    win.sel = 1;
                    win.setVisible(true);
                } else if (temp.substring(9).equals("P")) {
                    EscolheCor win = new EscolheCor(this.owner);
                    win.jColorChooser.setColor(Corre.dat.corPos);
                    win.sel = 2;
                    win.setVisible(true);
                } else if (temp.substring(9).equals("N")) {
                    EscolheCor win = new EscolheCor(this.owner);
                    win.jColorChooser.setColor(Corre.dat.corNeg);
                    win.sel = 3;
                    win.setVisible(true);
                } else if (temp.substring(9).equals("T")) {
                    EscolheCor win = new EscolheCor(this.owner);
                    win.jColorChooser.setColor(Corre.dat.texto);
                    win.sel = 4;
                    win.setVisible(true);
                } else if (temp.substring(9).equals("O")) {
                    EscolheCor win = new EscolheCor(this.owner);
                    win.jColorChooser.setColor(Corre.dat.corPotencial);
                    win.sel = 5;
                    win.setVisible(true);
                }
            }
        } else if (temp.equals("RESET")) {
            Corre.dat.limpaTUDO();
            this.setVars();
        }

        updateColors();
    }

    private JButton getButton() {
        if (button == null) {
            button = new JButton("Change");
            button.setName("Background Color Change JButton");
            button.setActionCommand("PICKCOLORB"); // Generated
            button.addActionListener(this);
        }

        return button;
    }

    private JButton getButton1() {
        if (button1 == null) {
            button1 = new JButton("mudar");
            button1.setName("Potential Line Color Change JButton");
            button1.setActionCommand("PICKCOLORO");
            button1.addActionListener(this);
        }

        return button1;
    }

    private JButton getButton3() {
        if (button3 == null) {
            button3 = new JButton("mudar");
            button3.setName("carga positiva");
            button3.setActionCommand("PICKCOLORP"); // Generated
            button3.addActionListener(this);
        }

        return button3;
    }

    private JButton getButton4() {
        if (button4 == null) {
            button4 = new JButton("mudar");
            button4.setName("carga negativa");
            button4.setActionCommand("PICKCOLORN"); // Generated
            button4.addActionListener(this);
        }

        return button4;
    }

    private JButton getButton5() {
        if (button5 == null) {
            button5 = new JButton("mudar");
            button5.setName("texto carga");
            button5.setActionCommand("PICKCOLORT"); // Generated
            button5.addActionListener(this);
        }

        return button5;
    }

    private JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton("Cancelar");
            cancelButton.setActionCommand("CANCELAR"); // Generated
            cancelButton.addActionListener(this);
        }

        return cancelButton;
    }

    private Choice getChoice() {
        if (choice == null) {
            choice = new Choice();
            choice.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    Corre.dat.setPrefixo(choice.getSelectedIndex());
                }
            });

            for (int i = 0; i < 9; i++) {
                choice.add(GuardaInfo.prefixos[i].toString());
            }
            choice.select(Corre.dat.getIndicePrefixo());
        }
        return choice;
    }

    private JPanel getColorPanel() {
        if (colorPanel == null) {
            label8 = new JLabel();
            potColor = new JLabel();
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
            label5 = new JLabel();
            bgColor = new JLabel("\t");
            posCharge = new JLabel("\t");
            cargaNeg = new JLabel("\t");
            textColor = new JLabel("\t");
            label4 = new JLabel();
            label3 = new JLabel();
            label2 = new JLabel();
            label1 = new JLabel();
            GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
            colorPanel = new JPanel();
            colorPanel.setLayout(new GridBagLayout());
            gridBagConstraints3.gridx = 0;
            gridBagConstraints3.gridy = 1;
            label1.setText("Background:");
            gridBagConstraints4.gridx = 2;
            gridBagConstraints4.gridy = 1;
            gridBagConstraints5.gridx = 1;
            gridBagConstraints5.gridy = 1;
            gridBagConstraints6.gridx = 0;
            gridBagConstraints6.gridy = 2;
            label2.setText("Postive Charge:");
            gridBagConstraints7.gridx = 1;
            gridBagConstraints7.gridy = 2;
            gridBagConstraints8.gridx = 2;
            gridBagConstraints8.gridy = 2;
            gridBagConstraints9.gridx = 0;
            gridBagConstraints9.gridy = 3;
            label3.setText("Negtive Charge:");
            gridBagConstraints10.gridx = 1;
            gridBagConstraints10.gridy = 3;
            gridBagConstraints12.gridx = 0;
            gridBagConstraints12.gridy = 4;
            label4.setText("Text:");
            gridBagConstraints13.gridx = 1;
            gridBagConstraints13.gridy = 4;
            gridBagConstraints5.insets = new java.awt.Insets(1, 2, 1, 2);
            gridBagConstraints7.insets = new java.awt.Insets(1, 2, 1, 2);
            gridBagConstraints10.insets = new java.awt.Insets(1, 2, 1, 2);
            gridBagConstraints3.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints6.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints9.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints12.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints13.insets = new java.awt.Insets(1, 2, 1, 2);
            gridBagConstraints14.gridx = 2;
            gridBagConstraints14.gridy = 4;
            gridBagConstraints11.gridx = 2;
            gridBagConstraints11.gridy = 3;
            label5.setText("Potential Resolution:");
            gridBagConstraints1.gridx = 2;
            gridBagConstraints1.gridy = 5;
            gridBagConstraints1.insets = new java.awt.Insets(0, 0, 1, 0);
            gridBagConstraints2.gridx = 1;
            gridBagConstraints2.gridy = 5;
            gridBagConstraints2.insets = new java.awt.Insets(0, 0, 1, 0);
            gridBagConstraints31.gridx = 0;
            gridBagConstraints31.gridy = 5;
            gridBagConstraints31.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints31.insets = new java.awt.Insets(0, 0, 1, 0);
            label8.setText("Potential Line");
            colorPanel.add(bgColor, gridBagConstraints5);
            colorPanel.add(posCharge, gridBagConstraints7);
            colorPanel.add(cargaNeg, gridBagConstraints10);
            colorPanel.add(textColor, gridBagConstraints13);
            colorPanel.add(label1, gridBagConstraints3);
            colorPanel.add(label2, gridBagConstraints6);
            colorPanel.add(label3, gridBagConstraints9);
            colorPanel.add(label4, gridBagConstraints12);
            colorPanel.add(getButton(), gridBagConstraints4);
            colorPanel.add(getButton3(), gridBagConstraints8);
            colorPanel.add(getButton5(), gridBagConstraints14);
            colorPanel.add(getButton4(), gridBagConstraints11);
            colorPanel.add(getButton1(), gridBagConstraints1);
            colorPanel.add(potColor, gridBagConstraints2);
            colorPanel.add(label8, gridBagConstraints31);
            colorPanel.addKeyListener(this);
        }

        return colorPanel;
    }

    private JTextField getGradientMax() {
        if (gradientMax == null) {
            gradientMax = new JTextField();
            gradientMax.addKeyListener(this);
            gradientMax.setInputVerifier(new VerificaDouble());
        }

        return gradientMax;
    }

    private JTextField getGradientMin() {
        if (gradientMin == null) {
            gradientMin = new JTextField();
            gradientMin.addKeyListener(this);
            gradientMin.setInputVerifier(new VerificaDouble());
        }

        return gradientMin;
    }

    private JCheckBox getGridOnOff() {
        if (gridOnOff == null) {
            gridOnOff = new JCheckBox();
        }
        return gridOnOff;
    }

    private JTextField getGridSpace() {
        if (gridSpace == null) {
            gridSpace = new JTextField();
            gridSpace.setInputVerifier(new VerificaDouble());
        }

        return gridSpace;
    }

    private JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton("OK");
            okButton.addActionListener(this);
        }
        return okButton;
    }

    private JPanel getOkPanel() {
        if (okPanel == null) {
            okPanel = new JPanel();
            okPanel.add(getOkButton(), null);
            okPanel.add(getCancelButton(), null);
            okPanel.add(getReset(), null);
        }

        return okPanel;
    }

    private JPanel getOptionsPanel() {
        if (optionsPanel == null) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.gridx = 2;
            label15 = new JLabel();
            label14 = new JLabel();
            GridBagConstraints gridBagConstraints17 = new GridBagConstraints();
            label13 = new JLabel();
            label12 = new JLabel();
            label11 = new JLabel();
            label10 = new JLabel();
            GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints18 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints24 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints23 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints33 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints32 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints42 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints53 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints63 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints73 = new GridBagConstraints();
            label9 = new JLabel();
            GridBagConstraints gridBagConstraints62 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints72 = new GridBagConstraints();
            label7 = new JLabel();
            GridBagConstraints gridBagConstraints41 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints52 = new GridBagConstraints();
            label6 = new JLabel();
            GridBagConstraints gridBagConstraints81 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints91 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints101 = new GridBagConstraints();
            label = new JLabel();
            GridBagConstraints gridBagConstraints51 = new GridBagConstraints();
            GridBagConstraints gridBagConstraints71 = new GridBagConstraints();
            optionsPanel = new JPanel();
            optionsPanel.setLayout(new GridBagLayout());
            gridBagConstraints51.gridx = 0;
            gridBagConstraints51.gridy = 0;
            gridBagConstraints51.gridwidth = 2;
            gridBagConstraints51.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints51.insets = new java.awt.Insets(0, 0, 1, 0);
            gridBagConstraints71.gridx = 1;
            gridBagConstraints71.gridy = 1;
            gridBagConstraints71.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints71.insets = new java.awt.Insets(0, 0, 1, 0);
            label.setText("Gradient Min");
            gridBagConstraints81.gridx = 2;
            gridBagConstraints81.gridy = 1;
            gridBagConstraints81.weightx = 1.0;
            gridBagConstraints81.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints91.gridx = 1;
            gridBagConstraints91.gridy = 2;
            gridBagConstraints91.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints91.insets = new java.awt.Insets(0, 0, 1, 0);
            label6.setText("Gradient Max");
            gridBagConstraints101.gridx = 2;
            gridBagConstraints101.gridy = 2;
            gridBagConstraints101.weightx = 1.0;
            gridBagConstraints101.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints41.gridx = 1;
            gridBagConstraints41.gridy = 3;
            gridBagConstraints41.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints41.insets = new java.awt.Insets(0, 0, 1, 0);
            label7.setText("# Potential Lines");
            gridBagConstraints52.gridx = 2;
            gridBagConstraints52.gridy = 3;
            gridBagConstraints52.weightx = 1.0;
            gridBagConstraints52.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints62.gridx = 1;
            gridBagConstraints62.gridy = 9;
            gridBagConstraints62.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints62.insets = new java.awt.Insets(0, 0, 1, 0);
            label9.setText("Grid on/off");
            gridBagConstraints72.gridx = 2;
            gridBagConstraints72.gridy = 9;
            gridBagConstraints72.anchor = java.awt.GridBagConstraints.WEST;
            gridBagConstraints15.gridx = 1;
            gridBagConstraints15.gridy = 5;
            gridBagConstraints15.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints15.insets = new java.awt.Insets(0, 0, 1, 0);
            label10.setText("Potential Spacing (V)");
            gridBagConstraints21.gridx = 2;
            gridBagConstraints21.gridy = 5;
            gridBagConstraints21.weightx = 1.0;
            gridBagConstraints21.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints32.gridx = 1;
            gridBagConstraints32.gridy = 6;
            gridBagConstraints32.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints32.insets = new java.awt.Insets(0, 0, 1, 0);
            label11.setText("Potential Offset (V)");
            gridBagConstraints42.gridx = 1;
            gridBagConstraints42.gridy = 7;
            gridBagConstraints42.anchor = java.awt.GridBagConstraints.EAST;
            gridBagConstraints42.insets = new java.awt.Insets(0, 0, 1, 0);
            label12.setText("Pixels per meter");
            gridBagConstraints53.gridx = 2;
            gridBagConstraints53.gridy = 6;
            gridBagConstraints53.weightx = 1.0;
            gridBagConstraints53.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints63.gridx = 2;
            gridBagConstraints63.gridy = 7;
            gridBagConstraints63.weightx = 1.0;
            gridBagConstraints63.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints73.gridx = 2;
            gridBagConstraints73.gridy = 5;
            gridBagConstraints73.weightx = 1.0;
            gridBagConstraints73.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints22.gridx = 1;
            gridBagConstraints22.gridy = 8;
            label13.setText("Grid Spacing (Pixels)");
            gridBagConstraints33.gridx = 2;
            gridBagConstraints33.gridy = 8;
            gridBagConstraints33.weightx = 1.0;
            gridBagConstraints33.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints17.gridx = 2;
            gridBagConstraints17.gridy = 11;
            gridBagConstraints23.gridx = 1;
            gridBagConstraints23.gridy = 11;
            label14.setText("Charge Prefix:");
            gridBagConstraints18.gridx = 1;
            gridBagConstraints18.gridy = 10;
            gridBagConstraints18.anchor = java.awt.GridBagConstraints.EAST;
            label15.setText("Tick Marks on/off");
            gridBagConstraints24.gridx = 2;
            gridBagConstraints24.gridy = 10;
            gridBagConstraints24.anchor = java.awt.GridBagConstraints.WEST;
            optionsPanel.add(label5, gridBagConstraints51);
            optionsPanel.add(label, gridBagConstraints71);
            optionsPanel.add(getGradientMin(), gridBagConstraints81);
            optionsPanel.add(label6, gridBagConstraints91);
            optionsPanel.add(getGradientMax(), gridBagConstraints101);
            optionsPanel.add(label7, gridBagConstraints41);
            optionsPanel.add(getPLines(), gridBagConstraints52);
            optionsPanel.add(label9, gridBagConstraints62);
            optionsPanel.add(getGridOnOff(), gridBagConstraints72);
            optionsPanel.add(label10, gridBagConstraints15);
            optionsPanel.add(label11, gridBagConstraints32);
            optionsPanel.add(label12, gridBagConstraints42);
            optionsPanel.add(getPOff(), gridBagConstraints53);
            optionsPanel.add(getPSpace(), gridBagConstraints73);
            optionsPanel.add(getSiConv(), gridBagConstraints63);
            optionsPanel.add(label13, gridBagConstraints22);
            optionsPanel.add(getGridSpace(), gridBagConstraints33);
            optionsPanel.add(getChoice(), gridBagConstraints17);
            optionsPanel.add(label14, gridBagConstraints23);
            optionsPanel.add(label15, gridBagConstraints18);
            optionsPanel.add(getTickOnOff(), gridBagConstraints24);
            optionsPanel.add(getPotResField(), gridBagConstraints);
        }

        return optionsPanel;
    }

    private JTextField getPLines() {
        if (pLines == null) {
            pLines = new JTextField();
            pLines.addKeyListener(this);
            pLines.setInputVerifier(new VerificaDouble());
        }

        return pLines;
    }

    private JTextField getPOff() {
        if (pOff == null) {
            pOff = new JTextField();
            pOff.setInputVerifier(new VerificaDouble());
        }
        return pOff;
    }

    private JTextField getPotResField() {
        if (potResField == null) {
            potResField = new JTextField();
            potResField.setInputVerifier(new VerificaDouble());
        }
        return potResField;
    }

    private JTextField getPSpace() {
        if (pSpace == null) {
            pSpace = new JTextField();
            pSpace.setInputVerifier(new VerificaDouble());
        }

        return pSpace;
    }

    private JButton getReset() {
        if (reset == null) {
            reset = new JButton("Reset All Settings");
            reset.setActionCommand("RESET");
            reset.addActionListener(this);
        }
        return reset;
    }

    private JTextField getSiConv() {
        if (siConv == null) {
            siConv = new JTextField();
            siConv.setInputVerifier(new VerificaDouble());
        }
        return siConv;
    }

    private JCheckBox getTickOnOff() {
        if (tickOnOff == null) {
            tickOnOff = new JCheckBox();
        }
        return tickOnOff;
    }

    public void initialize() {
        this.setLayout(new BorderLayout());
        this.setTitle("Options");
        this.setSize(328, 475);
        this.setModal(true);
        this.add(getColorPanel(), java.awt.BorderLayout.NORTH);
        this.add(getOkPanel(), java.awt.BorderLayout.SOUTH);
        this.add(getOptionsPanel(), java.awt.BorderLayout.CENTER);
        this.addKeyListener(this);
        this.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent e) {
                updateColors();
            }
        });
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();
            }
        });
        this.setVars();
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }

        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                this.Sync();
                this.dispose();
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                this.Sync();
                this.dispose();
            }
        }
    }

    public void keyTyped(java.awt.event.KeyEvent e) {
        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                this.Sync();
                this.dispose();
            }
        }
    }

    public void setVars() {
        this.gradientMax.setText("" + Corre.dat.gradMax);
        this.gradientMin.setText("" + Corre.dat.gradMin);
        this.potResField.setText("" + Corre.dat.getPotencialLinhadeCarga());
        this.pLines.setText("" + Corre.dat.plinhas);
        this.pOff.setText("" + Corre.dat.potOffset);
        this.pSpace.setText("" + Corre.dat.potspacing);
        this.siConv.setText("" + Corre.dat.FactorSistemaInternacional);
        this.gridOnOff.setSelected(Corre.dat.grelha);
        this.tickOnOff.setSelected(Corre.dat.ticksOn);
        this.gridSpace.setText("" + Corre.dat.tickSpacing);
        background = Corre.dat.backgroundColor;
        poz = Corre.dat.corPos;
        neg = Corre.dat.corNeg;
        this.updateColors();
    }

    private void Sync() {
        Corre.dat.grelha = gridOnOff.isSelected();
        Corre.dat.ticksOn = tickOnOff.isSelected();
        Corre.dat.plinhas = Integer.parseInt(pLines.getText());
        Corre.dat.setPotencialLinhaDeCarga(Integer.parseInt(potResField.getText()));
        Corre.dat.gradMax = Long.parseLong(gradientMax.getText());
        Corre.dat.gradMin = Long.parseLong(gradientMin.getText());
        Corre.dat.potOffset = Long.parseLong(pOff.getText());
        Corre.dat.potspacing = Long.parseLong(pSpace.getText());
        Corre.dat.FactorSistemaInternacional = Double.parseDouble(siConv.getText());
        Corre.dat.tickSpacing = Integer.parseInt(gridSpace.getText());
        Corre.janela.atualizaLista();
    }

    public void updateColors() {
        this.potColor.setBackground(Corre.dat.corPotencial);
        this.bgColor.setBackground(Corre.dat.backgroundColor);
        this.posCharge.setBackground(Corre.dat.corPos);
        this.cargaNeg.setBackground(Corre.dat.corNeg);
        this.textColor.setBackground(Corre.dat.texto);
        this.potColor.repaint();
        this.textColor.repaint();
        this.bgColor.repaint();
        this.posCharge.repaint();
        this.cargaNeg.repaint();
    }
} // @jve:decl-index=0:visual-constraint="10,10"
