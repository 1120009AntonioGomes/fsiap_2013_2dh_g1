package gui;

import Calculadora.IGcalculadora;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.ListSelectionModel;

import calc.Calculo;
import calc.Carga;
import calc.Corre;

public class Main extends JFrame implements ActionListener, KeyListener {

    private JButton botaoAdicionarCarga = null, botaoEditar = null,
            botaoLimpar = null, botaoRemover = null, botaoReDesenhar = null;

    public Color corFundo = null;

    private JList ListaCargas = null;

    private JPanel LimpaReDesenha = null;

    public JPanel controlo = null, PainelPai = null, PainelNumerico = null,
            pintarPainel = null, PainelBotoes = null, PainelBaixo = null,
            PainelAuxiliar = null, PainelEstado = null;

    public Carga cargaCorrente;

    public FPanel FPainel = null;

    private JCheckBox caixaLinhasCampo = null, poteLinhas = null;

    private JSlider fieldLineSlider = null;

    private URL imagem = null;

    private JLabel label = null, labelNumLinhas = null, labelPotencial = null,
            labelForca = null, poteR = null, labelDistancia = null;

    private JScrollPane painelLista = null;

    private JMenuItem menuAdiciona = null, menuEdita = null,
            menuRemove = null, menuRemoveTodos = null,
            menuSai = null, menuCarrega = null, menuOpcoes = null,
            menuConfig = null, menuImpressao = null,
            menuSalva = null, menuAjuda = null;

    private JMenuBar barra;

    private JMenu menuficheiro = null, jmenuajuda = null, menuacoes = null;

    public JanelaOpcoes janela = null;

    protected ArrayList<Double> clique = new ArrayList<Double>();

    private JMenuItem addLinha = null, addPonto = null, ddesenho = null,
            eedicao = null, rremocao = null;

    private JPopupMenu menuAlternativo = null, menuSobreposto = null;

    private JSlider slider = null;

    private int x2, y2;

    public Main() {
        super();
        initialize();
        this.setExtendedState(6);
    }

    public void actionPerformed(ActionEvent arg0) {
        String actCmd = arg0.getActionCommand();
        if (actCmd.equals("SAIR")) {
            Corre.Exit(0);
        } else if (actCmd.equals("ADICIONAR")) {
            AdicionarCarga a = new AdicionarCarga(this);
            a.setVisible(true);
            atualizaLista();
            reDraw();
        } else if (actCmd.equals("REMOVER")) {
            Corre.dat.removeCarga(cargaCorrente);
            this.nextCurrent();
            Corre.dat.actualizaEscalas();
            atualizaLista();
            reDraw();
        } else if (actCmd.equals("SOBRE")) {//Fazer janela
           //chamar janela com informacao aqui
        } else if (actCmd.equals("EDITAR") && cargaCorrente != null) {
            EditaCarga optwin = new EditaCarga(this);
            optwin.setVisible(true);
        } else if (actCmd.equals("DESENHAR AQUI")) {
            clique.add(new Double(Calculo.EnergiaPotencialNumPonto(x2, y2)));
            reDraw();
        } else if (actCmd.equals("ADICIONAR PONTO")) {
            AdicionarCarga a = new AdicionarCarga(this, x2, y2);
            a.setVisible(true);
            atualizaLista();
            reDraw();
        } else if (actCmd.equals("ADICIONAR LINHA AQUI")) {
            AdicionarCarga a = new AdicionarCarga(this, x2, y2);
            a.setVisible(true);
            atualizaLista();
            reDraw();
        } else if (actCmd.equals("OPCOES")) {
            janela = new JanelaOpcoes(this);
            janela.setVisible(true);
        } else if (actCmd.equals("REMOVER TODOS")) {
            RemoveTodos optwin = new RemoveTodos(this);
            optwin.setVisible(true);
            reDraw();
        } else if (actCmd.equals("CARREGAR")) {
            CarregaDefinicoes optwin = new CarregaDefinicoes(this, "CARREGA", CarregaDefinicoes.LOAD);
            optwin.setVisible(true);

            if (optwin.getFile() != null) {
                Corre.dat = Corre.Load(optwin.getFile());
                Corre.dat.rebuildAllShapes();
                Corre.dat.actualizaEscalas();
                this.actualizaTodos();
                this.reDraw();
            }
        } else if (actCmd.equals("SALVAR")) {
            CarregaDefinicoes win = new CarregaDefinicoes(this, "Salvar", CarregaDefinicoes.SAVE);
            win.setVisible(true);

            if (win.getFile() != null) {
                Corre.Salva(win.getFile());
            }
        }
    }

    private void chargeMove(java.awt.event.MouseEvent e) {
        if (cargaCorrente != null) {
            cargaCorrente.setX(e.getX());
            cargaCorrente.setY(e.getY());
            cargaCorrente.drawOutline(FPainel.getGraphics());
        }
    }

    private boolean checkSel(java.awt.event.MouseEvent e) {
        boolean encontrou = false;
        for (Carga temp : Corre.dat.getCargas()) {
            if (temp.verificaClicado(e)) {
                temp.isSel = true;
                ListaCargas.setSelectedIndex(Corre.dat.getIndiceCarga(temp));
                cargaCorrente = temp;
                actualizaEtiquetas();
                encontrou = true;
            }
        }

        if (!encontrou) {
            cargaCorrente = null;
        }
        for (Carga temp : Corre.dat.getCargas()) {
            if (temp == cargaCorrente) {
                temp.isSel = true;
            } else {
                temp.isSel = false;
            }
        }
        return encontrou;
    }

    private JPanel getPainelAuxiliar() {
        if (PainelAuxiliar == null) {
            labelPotencial = new JLabel();
            labelForca = new JLabel();
            PainelAuxiliar = new JPanel();
            this.addKeyListener(this);
            PainelAuxiliar.setLayout(new GridBagLayout());
            labelForca.setText("Adicionar carga para calcular a forca");
            labelPotencial.setText("Andar com o rato para saber o potencial");
        }

        return PainelAuxiliar;
    }

    private JPanel getPainelEmBaixo() {
        if (PainelBaixo == null) {
            PainelBaixo = new JPanel();
            PainelBaixo.addKeyListener(this);
            PainelBaixo.setLayout(new BorderLayout());
            PainelBaixo.add(getPainelAuxiliar(), BorderLayout.NORTH);
            PainelBaixo.add(getPainelEstado(), BorderLayout.SOUTH);
            PainelBaixo.add(getPainelNumerico(), BorderLayout.WEST);
        }
        return PainelBaixo;
    }

    private JPanel getPainelBotoes() {
        if (PainelBotoes == null) {
            PainelBotoes = new JPanel();
            PainelBotoes.setName("Painel de Botoes");
            PainelBotoes.setPreferredSize(new java.awt.Dimension(313, 136));
            PainelBotoes.add(getButaoEditar(), null);
        }
        return PainelBotoes;
    }

    private JList getListaCargas() {
        if (ListaCargas == null) {
            ListaCargas = new JList(Corre.dat.getCargas().toArray());
            ListaCargas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            ListaCargas
                    .addListSelectionListener(new javax.swing.event.ListSelectionListener() {
                        public void valueChanged(
                                javax.swing.event.ListSelectionEvent e) {
                                    if (cargaCorrente != null) {
                                        cargaCorrente.isSel = false;
                                    }
                                    int index = ListaCargas.getSelectedIndex();
                                    if (index > 0
                                    && index < Corre.dat.getNumCargas()) {
                                        cargaCorrente = Corre.dat.getCarga(index);
                                        cargaCorrente.isSel = true;
                                    } else {
                                        cargaCorrente = null;
                                    }
                                    FPainel.repaint();
                                }
                    });
            ListaCargas.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent e) {
                    if (e.getButton() != 1) {
                        menuSobreposto.show(ListaCargas, e.getX(), getY());
                    }
                }
            });
        }
        return ListaCargas;
    }

    private JPanel getControlo() {
        if (controlo == null) {
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints2.gridy = 2;
            gridBagConstraints2.weightx = 1.0;
            gridBagConstraints2.weighty = 1.0;
            gridBagConstraints2.gridx = 0;
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.gridy = 1;
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            controlo = new JPanel();
            this.addKeyListener(this);
            controlo.setLayout(new GridBagLayout());
            controlo.setPreferredSize(new java.awt.Dimension(208, 203));
            controlo.setMinimumSize(new java.awt.Dimension(130, 203));
            controlo.add(getPainelBotoes(), gridBagConstraints);
            controlo.add(getPintarPainel(), gridBagConstraints1);
            controlo.add(getListScrollPane(), gridBagConstraints2);
        }

        return controlo;
    }

    private JButton getButaoEditar() {
        if (botaoEditar == null) {
            botaoEditar = new JButton("Editar");
            botaoEditar.setActionCommand("EDITAR");
            botaoEditar.addActionListener(this);
        }

        return botaoEditar;
    }

    public int getAlturaPainel() {
        return FPainel.getHeight();
    }

    private FPanel getFPanel() {
        if (FPainel == null) {
            FPainel = new FPanel();
            FPainel.addComponentListener(new java.awt.event.ComponentAdapter() {
                public void componentResized(java.awt.event.ComponentEvent e) {
                    FPainel.resetSize();
                    reDraw();
                }
            });
            FPainel.add(this.menuAlternativo);
            FPainel.add(this.menuSobreposto);

            FPainel.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    boolean temp = checkSel(e);
                    if (e.getButton() != 1) {
                        if (temp) {
                            menuSobreposto.show(FPainel, e.getX(), e.getY());
                        } else {
                            x2 = e.getX();
                            y2 = e.getY();
                            menuAlternativo.show(FPainel, e.getX(), e.getY());
                        }
                    }
                    FPainel.repaint();
                }

                @Override
                public void mouseReleased(MouseEvent arg0) {
                    actualizaEtiquetas();
                    FPainel.repaint();
                    reDraw();
                }
            });
            FPainel.addMouseMotionListener(new MouseMotionAdapter() {
                public void mouseDragged(MouseEvent e) {
                    checkSel(e);
                    if (e.getButton() != 0) {
                        return;
                    }
                    chargeMove(e);
                    atualizaLista();
                }

                public void mouseMoved(MouseEvent e) {
                    double[] data = Calculo.ForcaTotaleAngulo(e.getX(), e.getY());
                    String force = "" + Calculo.truncaParaString(data[0], 2);
                    String degree = "" + data[1];
                    try {
                        degree = degree.substring(0, 5);
                    } catch (RuntimeException e1) {
                        degree = degree.substring(0, 3);
                    }
                    String temp = force + " Newtons @ " + degree + "º";
                    labelForca.setText(temp);
                    String pot = Calculo.truncaParaString(calc.Calculo.EnergiaPotencialNumPonto(e
                            .getX(), e.getY()), 2);
                    labelPotencial.setText(pot + " Volts");
                    if (cargaCorrente == null) {
                        labelDistancia.setText("");
                    } else {
                        if (cargaCorrente != null) {
                            double dist = Calculo.dist(e.getX(), e.getY(),
                                    cargaCorrente.getX(), cargaCorrente.getY());
                            dist /= Corre.dat.FactorSistemaInternacional;
                            dist /= Corre.dat.getEscala();
                            labelDistancia.setText("Distancia: "
                                    + Calculo.truncaParaString(dist, 2) + "m");
                        }
                    }
                }
            });
        }
        return FPainel;
    }

    public int getLarguraPainel() {
        return FPainel.getWidth();
    }

    private JCheckBox getCaixaLinhasCampo() {
        if (caixaLinhasCampo == null) {
            caixaLinhasCampo = new JCheckBox("Linhas Campo");
            caixaLinhasCampo.setMinimumSize(new java.awt.Dimension(70, 23));
            caixaLinhasCampo.setSelected(Corre.dat.fieldLinesOn);
            caixaLinhasCampo.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    Corre.dat.fieldLinesOn = caixaLinhasCampo.isSelected();
                    reDraw();
                }
            });
        }
        return caixaLinhasCampo;
    }

    private JSlider getFieldLineSlider() {
        if (fieldLineSlider == null) {
            fieldLineSlider = new JSlider();
            fieldLineSlider.setMinimum(0);
            fieldLineSlider.setPaintTicks(true);
            fieldLineSlider.setPaintLabels(true);
            fieldLineSlider
                    .setToolTipText("Move slider to set total number of Field Lines.");
            fieldLineSlider.setValue(Corre.dat.getDernsidadeCarga());
            fieldLineSlider.setMajorTickSpacing(20);
            fieldLineSlider.setMinorTickSpacing(5);
            fieldLineSlider
                    .addChangeListener(new javax.swing.event.ChangeListener() {
                        public void stateChanged(javax.swing.event.ChangeEvent e) {
                            Corre.dat.setDensidadeCarga(fieldLineSlider.getValue());
                            reDraw();
                        }
                    });
        }
        return fieldLineSlider;
    }

    private JScrollPane getListScrollPane() {
        if (painelLista == null) {
            painelLista = new JScrollPane();
            painelLista.setAutoscrolls(true);
            painelLista.setViewportView(getListaCargas());
        }
        return painelLista;
    }

    private JPanel getPainelPrincipal() {
        if (PainelPai == null) {
            label = new JLabel();
            PainelPai = new JPanel();
            PainelPai.setLayout(new BorderLayout());
            label.setText("Carregar...");
            PainelPai.add(getControlo(), java.awt.BorderLayout.EAST);
            PainelPai.add(label, java.awt.BorderLayout.NORTH);
            PainelPai.add(getPainelEmBaixo(), java.awt.BorderLayout.SOUTH);
            PainelPai.add(getFPanel(), java.awt.BorderLayout.CENTER);
            PainelPai.addKeyListener(this);

        }

        return PainelPai;
    }

    private JMenuItem getMenuRemoveTodos() {
        if (menuRemoveTodos == null) {

        }
        return menuRemoveTodos;
    }

    private JPanel getPainelNumerico() {
        if (PainelNumerico == null) {
            poteR = new JLabel();
            poteR.setText("Resolucao Potencial:");
            labelNumLinhas = new JLabel();
            labelNumLinhas.setText("Densidade da Linha de Campo");
            PainelNumerico = new JPanel();
            PainelNumerico.add(labelNumLinhas, null);
            PainelNumerico.add(getFieldLineSlider(), null);
            PainelNumerico.add(poteR, null);
            PainelNumerico.add(getSlider(), null);
        }
        return PainelNumerico;
    }

    private JPanel getPintarPainel() {
        if (pintarPainel == null) {
            pintarPainel = new JPanel();
            pintarPainel.setMinimumSize(new java.awt.Dimension(300, 60));
            pintarPainel.setPreferredSize(new java.awt.Dimension(196, 13));
            pintarPainel.add(getCaixaLinhasCampo(), null);
            pintarPainel.add(getConjuntoLinhas(), null);
        }
        return pintarPainel;
    }

    private JCheckBox getConjuntoLinhas() {
        if (poteLinhas == null) {
            poteLinhas = new JCheckBox("Linhas Equipotenciais");
            poteLinhas.setSelected(Corre.dat.potentialLinesOn);
            poteLinhas.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    Corre.dat.potentialLinesOn = poteLinhas.isSelected();
                    reDraw();
                }
            });
        }
        return poteLinhas;
    }

    private JSlider getSlider() {
        if (slider == null) {
            slider = new JSlider();
            slider.setSize(new java.awt.Dimension(198, 56));
            slider.setMajorTickSpacing(3);
            slider.setMaximum(16);
            slider.setMinimum(1);
            slider.setPaintLabels(true);
            slider.setPaintTicks(true);
            slider.setPaintTrack(true);
            slider
                    .setToolTipText("mude a resolucao da linha potencial");
            slider.setValue(Corre.dat.getPotencialLinhadeCarga());
            slider.setInverted(false);
            slider
                    .addChangeListener(new javax.swing.event.ChangeListener() {
                        public void stateChanged(javax.swing.event.ChangeEvent e) {
                            Corre.dat.setPotencialLinhaDeCarga(slider.getValue());
                            reDraw();
                        }
                    });
        }
        return slider;
    }

    private JButton getBotaoReDesenhar() {
        if (botaoReDesenhar == null) {
            botaoReDesenhar = new JButton("Redesenha");
            botaoReDesenhar.setActionCommand("REDESENHA");
            botaoReDesenhar.addActionListener(this);
        }
        return botaoReDesenhar;
    }

    private JButton getBotaoRemover() {
        if (botaoRemover == null) {
            botaoRemover = new JButton("Remove");
            botaoRemover.setActionCommand("REMOVE");
            botaoRemover.addActionListener(this);
        }
        return botaoRemover;
    }

    private JPanel getPainelEstado() {
        if (PainelEstado == null) {
            labelDistancia = new JLabel();
            PainelEstado = new JPanel();
            PainelEstado.addKeyListener(this);
            PainelEstado.setLayout(new BorderLayout());
            labelDistancia.setText("Adicione Carga");
            PainelEstado.add(labelForca, java.awt.BorderLayout.WEST);
            PainelEstado.add(labelPotencial, java.awt.BorderLayout.EAST);
            PainelEstado.add(labelDistancia, java.awt.BorderLayout.CENTER);
        }

        return PainelEstado;
    }

    private void initialize() {
        this.corFundo = Corre.dat.backgroundColor;
        this.imagem = Main.class.getResource("logoGrupo.png"); //nao esquecer criar Logo

        this.setName("Janela Principal");
        this.setTitle("Carga Pontual em Campo Eletrico ");

        this.addKeyListener(this);
        this.setPreferredSize(new java.awt.Dimension(Corre.dat.mainWinWidth,
                Corre.dat.MainWinHeight));
        this.setMinimumSize(new java.awt.Dimension(Corre.dat.mainWinWidth,
                Corre.dat.MainWinHeight));
        this.setSize(Corre.dat.mainWinWidth, Corre.dat.MainWinHeight);
        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                FPainel.resetSize();
                if (getHeight() < 300) {
                    setSize(300, getWidth());
                }
                if (getWidth() < 500) {
                    setSize(getHeight(), 500);
                }
                Corre.dat.MainWinHeight = getHeight();
                Corre.dat.mainWinWidth = getWidth();
            }
        });
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Corre.Exit(0);
            }
        });
        this.setJMenuBar(preparaJMenu());
        this.preparaJMenuSobreposto();
        this.add(getPainelPrincipal());
        this.atualizaLista();
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 127) {
            Corre.dat.removeCarga(cargaCorrente);
            this.nextCurrent();
            atualizaLista();
            FPainel.repaint();
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 127) {
            Corre.dat.removeCarga(cargaCorrente);
            this.nextCurrent();
            atualizaLista();
            FPainel.repaint();
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getKeyCode() == 127) {
            Corre.dat.removeCarga(cargaCorrente);
            this.nextCurrent();
            atualizaLista();
            FPainel.repaint();
            ;
        }
    }

    private void nextCurrent() {
        for (Carga temp : Corre.dat.getCargas()) {
            if (temp != null) {
                cargaCorrente = temp;
                return;
            }
        }
    }

    public boolean reDraw() {
        FPainel.drawOffScreen();
        FPainel.repaint();
        return true;
    }

    public void removeAll() {
        for (int i = 0; i < Corre.dat.getNumCargas(); i++) {
            Corre.dat.limpaCargas();
        }

        cargaCorrente = null;
        atualizaLista();
        FPainel.repaint();
    }

    public void setStatusLabel(String text) {
        label.setText(text);
    }

    private JMenuBar preparaJMenu() {
        barra = new JMenuBar();
        menuficheiro = new JMenu("Ficheiro");
        menuSai = new JMenuItem("Sair");
        menuSai.setActionCommand("SAIR");
        menuSai.addActionListener(this);
        menuOpcoes = new JMenuItem("Opcoes...");
        menuOpcoes.setActionCommand("OPCOES");
        menuOpcoes.addActionListener(this);
        menuCarrega = new JMenuItem("Carregar...");
        menuCarrega.setActionCommand("CARREGAR");
        menuCarrega.addActionListener(this);
        menuConfig = new JMenuItem("Config");
        menuConfig.setActionCommand("CONFIG");
        menuConfig.addActionListener(this);
        menuImpressao = new JMenuItem("Imprimir...");
        menuImpressao.setActionCommand("IMPRIMIR");
        menuImpressao.addActionListener(this);
        menuSalva = new JMenuItem("Salvar...");
        menuSalva.setActionCommand("SALVAR");
        menuSalva.addActionListener(this);
        menuacoes = new JMenu("Acoes");
        menuAdiciona = new JMenuItem("Adicionar...");
        menuAdiciona.setActionCommand("ADICIONAR");
        menuAdiciona.addActionListener(this);
        menuRemove = new JMenuItem("Remover");
        menuRemove.setActionCommand("REMOVER");
        menuRemove.addActionListener(this);
        menuRemoveTodos = new JMenuItem("Remover Todos");
        menuRemoveTodos.setActionCommand("REMOVER TODOS");
        menuRemoveTodos.addActionListener(this);
        menuEdita = new JMenuItem("Editar...");
        menuEdita.setActionCommand("EDITAR");
        menuEdita.addActionListener(this);
        jmenuajuda = new JMenu("Ajuda");
        menuAjuda = new JMenuItem("Sobre...");
        menuAjuda.setActionCommand("SOBRE");
        menuAjuda.addActionListener(this);
     //   menuficheiro.add(menuOpcoes);
        menuficheiro.insertSeparator(2);
        menuficheiro.add(menuCarrega);
        menuficheiro.add(menuSalva);
        menuficheiro.insertSeparator(4);
        menuficheiro.add(menuSai);
        menuacoes.add(menuAdiciona);
        menuacoes.add(menuRemove);
        menuacoes.add(getMenuRemoveTodos());
        menuacoes.add(menuEdita);
        jmenuajuda.add(menuAjuda);
        barra.add(menuficheiro);
        barra.add(menuacoes);
        barra.add(jmenuajuda);

        return barra;
    }

    private void preparaJMenuSobreposto() {

        addLinha = new JMenuItem("Adicionar Linha de Potencial");
        addLinha.setActionCommand("ADICIONARLINHAAQUI");
        addLinha.addActionListener(this);
        addLinha.setEnabled(false);
        addPonto = new JMenuItem("Adicionar Carga Pontual");
        addPonto.setActionCommand("ADICIONARCARGAAQUI");
        addPonto.addActionListener(this);
        ddesenho = new JMenuItem("Desenhar Potencial");
        ddesenho.setActionCommand("DESENHARAQUI");
        ddesenho.addActionListener(this);
        eedicao = new JMenuItem("Editar...");
        eedicao.setActionCommand("EDITAR");
        eedicao.addActionListener(this);
        rremocao = new JMenuItem("Remover");
        rremocao.setActionCommand("REMOVER");
        rremocao.addActionListener(this);
        menuSobreposto = new JPopupMenu("MenuSobreposto");
        menuSobreposto.add(eedicao);
        menuSobreposto.add(rremocao);
        menuAlternativo = new JPopupMenu("Menu Sobreposto Alternativo");
        menuAlternativo.add(this.addPonto);
        menuAlternativo.add(this.addLinha);
        menuAlternativo.addSeparator();
        menuAlternativo.add(this.ddesenho);
    }

    public void actualizaTodos() {
        FPainel.drawOffScreen();
        FPainel.repaint();
        this.actualizaEtiquetas();
        this.atualizaLista();
    }

    private void actualizaEtiquetas() {
        if (cargaCorrente != null) {
            label.setText(cargaCorrente.toString());
        }
    }

    protected void atualizaLista() {
        ListaCargas.setListData(Corre.dat.getCargas().toArray());
    }

}
