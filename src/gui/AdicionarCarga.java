/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import calc.Carga;
import calc.Corre;
import calc.CargaPontual;

public class AdicionarCarga extends JDialog implements ActionListener, KeyListener {

    private JButton cancelButton = null;

    private PainelCarga cp = new PainelCarga();

    private JButton okButton = null;

    private Panel panel1 = null;

    public AdicionarCarga(java.awt.Frame owner) {
        super(owner);
        initialize();
        this.setLocationRelativeTo(owner);
    }

    public AdicionarCarga(java.awt.Frame owner, int x, int y) {
        super(owner);
        initialize();
        this.setLocationRelativeTo(owner);
        cp.setXY(x, y);
    }

    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getActionCommand().equals("OK")) {
            okButton.setEnabled(false);
            if (add()) {
                calc.Corre.janela.actualizaTodos();
                this.dispose();
            } else {
                okButton.setEnabled(true);
            }
        } else if (arg0.getActionCommand().equals("CANCEL")) {
            this.dispose();
        }
    }

    private boolean add() {
        Carga temp;
        try {
            temp = new CargaPontual(cp.getValorCarga(), cp.getXVal(), cp
                    .getYVal());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Error!\nPlease check fields and try again.");
            return false;
        }
        Corre.dat.adicionaCarga(temp);
        Corre.dat.actualizaEscalas();
        return true;
    }

    private JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton("Cancelar");
            cancelButton.addActionListener(this);
            cancelButton.setActionCommand("CANCELAR");
            cancelButton.setForeground(java.awt.Color.black);
        }
        return cancelButton;
    }

    private JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton("OK");
            okButton.addActionListener(this);
            okButton.setActionCommand("OK");
            okButton.setForeground(java.awt.Color.black);
            okButton.setEnabled(true);
        }
        return okButton;
    }

    private Panel getPanel1() {
        if (panel1 == null) {
            panel1 = new Panel();
            panel1.add(getOkButton(), null);
            panel1.add(getCancelButton(), null);
        }
        return panel1;
    }

    public void initialize() {
        this.setTitle("Adicionar Carga");
        this.setSize(331, 128);
        this.setModal(true);
        cp.setKeyListener(this);
        this.add(cp, java.awt.BorderLayout.NORTH);
        this.add(getPanel1(), java.awt.BorderLayout.SOUTH);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                okButton.setEnabled(false);
                add();
                calc.Corre.janela.actualizaTodos();
                this.dispose();
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                okButton.setEnabled(false);
                if (add()) {
                    calc.Corre.janela.actualizaTodos();
                    this.dispose();
                } else {
                    okButton.setEnabled(true);
                }
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (okButton.isEnabled()) {
                okButton.setEnabled(false);
                if (add()) {
                    calc.Corre.janela.actualizaTodos();
                    this.dispose();
                } else {
                    okButton.setEnabled(true);
                }
            }
        }
    }
} // @jve:decl-index=0:visual-constraint="21,21"
