/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import calc.Corre;

public class RedesenhaJanela extends Thread {

    /**
     * No arg constructor creates new thread as a daemon.
     */
    public RedesenhaJanela() {
        this.setDaemon(true);
        this.setName("Redesenha Janela!");
    }

    /**
     * Launches a new thread.
     */
    public void run() {
        if (Corre.janela.FPainel.rdtDraw()) {
            Corre.janela.FPainel.finishDraw();
        }
        Corre.janela.FPainel.repaint();
        System.gc();
    }
}
