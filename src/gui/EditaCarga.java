/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import calc.Corre;

public class EditaCarga extends JDialog implements ActionListener, KeyListener {

    private JButton applyButton = null;

    private JButton cancelButton = null;

    private PainelCarga cp = null;

    private JButton okButton = null;

    private JPanel panel2 = null;

    public EditaCarga(JFrame owner) {
        super(owner);
        if (Corre.janela.cargaCorrente == null) {
            this.dispose();
        }
        this.cp = new PainelCarga(Corre.janela.cargaCorrente);
        initialize();

    }

    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getActionCommand().equals("CANCEL")) {
            this.dispose();
            return;
        } else if (arg0.getActionCommand().equals("OK")) {
            if (cargaAlterada()) {
                calc.Corre.janela.actualizaTodos();
                this.dispose();
                Corre.janela.FPainel.repaint();
                return;
            }
        } else if (arg0.getActionCommand().equals("APPLY")) {
            if (cargaAlterada()) {
                cargaAlterada();
                calc.Corre.janela.actualizaTodos();
                Corre.janela.FPainel.repaint();
            }
        }
        Corre.dat.actualizaEscalas();
    }

    private boolean cargaAlterada() {
        try {
            Corre.janela.cargaCorrente.setCarga(cp.getValorCarga());
            Corre.janela.cargaCorrente.setX(cp.getXVal());
            Corre.janela.cargaCorrente.setY(cp.getYVal());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Error!\nPlease check fields and try again.");
            return false;
        }
        Corre.dat.actualizaEscalas();
        return true;
    }

    private JButton getApplyButton() {
        if (applyButton == null) {
            applyButton = new JButton("Alterar");
            applyButton.setName("Apply JButton");
            applyButton.setActionCommand("ALTERAR");
            applyButton.addActionListener(this);
        }
        return applyButton;
    }

    private JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton("Cancelar");
            cancelButton.setName("Cancel JButton");
            cancelButton.setActionCommand("CANCELAR");
            cancelButton.addActionListener(this);
        }
        return cancelButton;
    }

    private JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton("OK");
            okButton.setName("OK JButton");
            okButton.setActionCommand("OK");
            okButton.addActionListener(this);
        }
        return okButton;
    }

    private JPanel getPanel2() {
        if (panel2 == null) {
            panel2 = new JPanel();
            panel2.add(getOkButton(), null); // Generated
            panel2.add(getApplyButton(), null); // Generated
            panel2.add(getCancelButton(), null); // Generated
            panel2.addKeyListener(this);
        }
        return panel2;
    }

    public void initialize() {
        this.setSize(303, 141);
        this.setModal(true);
        cp.setKeyListener(this);
        this.add(cp, java.awt.BorderLayout.NORTH); // Generated
        this.add(getPanel2(), java.awt.BorderLayout.SOUTH); // Generated
        this.addKeyListener(this);
        this.setLocationRelativeTo(Corre.janela);
        this.setTitle("A editar " + Corre.janela.cargaCorrente);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (cargaAlterada()) {
                calc.Corre.janela.actualizaTodos();
                this.dispose();
                Corre.janela.FPainel.repaint();
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (cargaAlterada()) {
                calc.Corre.janela.actualizaTodos();
                this.dispose();
                Corre.janela.FPainel.repaint();
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getKeyCode() == 27) {
            this.dispose();
        }
        if (e.getKeyCode() == 10) {
            if (cargaAlterada()) {
                calc.Corre.janela.actualizaTodos();
                this.dispose();
                Corre.janela.FPainel.repaint();
            }
        }
    }
} // @jve:decl-index=0:visual-constraint="10,10"
