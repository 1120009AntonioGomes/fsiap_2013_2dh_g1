package gui;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

public class VerificaDouble extends InputVerifier {

    /**
     * Para certificar que recebe doubles
     */
    public boolean verify(JComponent arg0) {
        String text = ((JTextComponent) arg0).getText();
        if (text.equals("")) {
            return false;
        }
        try {
            Double.parseDouble(text);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
