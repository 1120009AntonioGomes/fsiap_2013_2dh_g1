/*
 * Aqui serão recebidos e apresentados os valores para o cálculo 
 * na expressão
 * 
 */
package Calculadora;

/**
 *
 * @author Fraga
 */
public class IOdados {
    private double forcalorenz;
    private double carga;
    private double campoE;
    private double velocidade;
    private double campoB;

    public IOdados() {
    }
    
    public IOdados(double forcalorenz, double carga, double campoE, double velocidade, double campoB) {
        this.forcalorenz = forcalorenz;
        this.carga = carga;
        this.campoE = campoE;
        this.velocidade = velocidade;
        this.campoB = campoB;
    }

    /**
     * @return the forcalorenz
     */
    public double getForcalorenz() {
        return forcalorenz;
    }

    /**
     * @param forcalorenz the forcalorenz to set
     */
    public void setForcalorenz(double forcalorenz) {
        this.forcalorenz = forcalorenz;
    }

    /**
     * @return the carga
     */
    public double getCarga() {
        return carga;
    }

    /**
     * @param carga the carga to set
     */
    public void setCarga(double carga) {
        this.carga = carga;
    }

    /**
     * @return the campoE
     */
    public double getCampoE() {
        return campoE;
    }

    /**
     * @param campoE the campoE to set
     */
    public void setCampoE(double campoE) {
        this.campoE = campoE;
    }

    /**
     * @return the velocidade
     */
    public double getVelocidade() {
        return velocidade;
    }

    /**
     * @param velocidade the velocidade to set
     */
    public void setVelocidade(double velocidade) {
        this.velocidade = velocidade;
    }

    /**
     * @return the campoB
     */
    public double getCampoB() {
        return campoB;
    }

    /**
     * @param campoB the campoB to set
     */
    public void setCampoB(double campoB) {
        this.campoB = campoB;
    }

    @Override
    public String toString() {
        return "campo magnetico " + campoB + "força de lorenz " + forcalorenz + " velocidade " + velocidade + " carga " + carga + "campo eletrico" + campoE ; //To change body of generated methods, choose Tools | Templates.      
    }
    
   
   
}
